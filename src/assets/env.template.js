(function (window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["ACCOUNT_MODULE_URL_PREFIX"] = "${ACCOUNT_MODULE_URL_PREFIX}";
  window["env"]["TEST_MODULE_URL_PREFIX"] = "${TEST_MODULE_URL_PREFIX}";
  window["env"]["ACHIEVEMENT_MODULE_URL_PREFIX"] = "${ACHIEVEMENT_MODULE_URL_PREFIX}";
  window["env"]["HOMEWORK_MODULE_URL_PREFIX"] = "${HOMEWORK_MODULE_URL_PREFIX}";
  window["env"]["ROADMAP_MODULE_URL_PREFIX"] = "${ROADMAP_MODULE_URL_PREFIX}";
})(this);
