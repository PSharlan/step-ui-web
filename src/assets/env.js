(function (window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window['env']?.TEST_MODULE_URL_PREFIX || 'http://localhost:8082/'
  window['env']?.ACCOUNT_MODULE_URL_PREFIX || 'http://localhost:8081/';
  window['env']?.HOMEWORK_MODULE_URL_PREFIX || 'http://localhost:8084/';
  window['env']?.ACHIEVEMENT_MODULE_URL_PREFIX || 'http://localhost:8087/';
  window['env']?.ROADMAP_MODULE_URL_PREFIX || 'http://localhost:8086/';
})(this);
