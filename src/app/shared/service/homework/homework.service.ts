import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Homework, HomeworkCreationRequest, HomeworkPreview, HomeworkWithAnswerPreview} from '../../model/homework/homework-models';
import {Observable} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';
import {Page} from '../../model/page-model';

const URL_PREFIX = URLPrefixService.getHomeworkModuleUrlPrefix();

@Injectable({providedIn: 'root'})

export class HomeworkService {

  homeworks: Homework[] = [];

  constructor(private http: HttpClient) {
  }

  create(request: HomeworkCreationRequest): Observable<Homework> {
    return this.http.post<Homework>(`${URL_PREFIX}homeworks`, request);
  }

  getPreviewByGroupId(id: number): Observable<Page<HomeworkPreview[]>> {
    return this.http.get<Page<HomeworkPreview[]>>(`${URL_PREFIX}students/groups/${id}/homeworks`);
  }

  getFullByGroupId(id: number): Observable<Page<Homework[]>> {
    return this.http.get<Page<Homework[]>>(`${URL_PREFIX}/teachers/groups/${id}/homeworks`);
  }

  getByGroupIdAndStudentId(groupId: number, studentId: number): Observable<Page<HomeworkWithAnswerPreview[]>> {
    return this.http.get<Page<HomeworkWithAnswerPreview[]>>(
      `${URL_PREFIX}/groups/${groupId}/students/${studentId}/homeworks`);
  }
}
