import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Answer, AnswerCreationRequest, AnswerUpdateRequest} from '../../model/homework/answer-models';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getHomeworkModuleUrlPrefix();

@Injectable({providedIn: 'root'})

export class HomeworkAnswerService {

  constructor(private http: HttpClient) {
  }

  create(request: AnswerCreationRequest): Observable<Answer> {
    return this.http.post<Answer>(`${URL_PREFIX}answers`, request);
  }

  getByHomeworkAndStudentId(homeworkId: number, studentId: number): Observable<Answer[]> {
    return this.http.get<Answer[]>(
      `${URL_PREFIX}answers?homeworkId=${homeworkId}&studentId=${studentId}`);
  }

  update(request: AnswerUpdateRequest): Observable<Answer> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Answer>(`${URL_PREFIX}answers`, request, {headers});
  }
}
