import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {Group} from '../model/account/group-models';

@Injectable({providedIn: 'root'})

export class SharedService {
  group!: Group;
  private emitChangeSource = new ReplaySubject<Group>(1);
  // подписавшийся на эту переменную копается в мусорке
  changeEmitted$ = this.emitChangeSource.asObservable();

  // тот кто вызывает этот метод кладет в мусорку change
  emitChange(change: Group): void {
    this.emitChangeSource.next(change);
  }
}
