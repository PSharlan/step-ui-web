import {Injectable} from '@angular/core';
import {NotificationService} from './notification.service';

@Injectable({providedIn: 'root'})

export class ErrorHandlerService {

  constructor(private notificationService: NotificationService) {
  }

  public handleError(errorResponse: any): void {
    let errorMessage;
    if (typeof (errorResponse) === 'string') {
      errorMessage = errorResponse;
    } else if (errorResponse.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${errorResponse.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${errorResponse.status} ${errorResponse.error.error}`;
    }
    this.notificationService.showErrorNotification(errorMessage);
  }
}
