import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Result, ResultCreationRequest, ResultPreview} from '../../model/test/result-models';
import {Observable, of} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpClient) {
  }

  create(request: ResultCreationRequest): Observable<Result> {
    return this.http.post<Result>(`${URL_PREFIX}results`, request);
  }

  getAll(page: number, size: number): Observable<ResultPreview[]> {
    return this.http.get<ResultPreview[]>(`${URL_PREFIX}results?page=${page}&size=${size}`);
  }

  getById(id: string): Observable<Result> {
    return this.http.get<Result>(`${URL_PREFIX}results/${id}`);
  }
}
