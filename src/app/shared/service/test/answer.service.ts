import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Answer, AnswerCreationRequest} from '../../model/test/answer-models';
import {Observable, of} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private http: HttpClient) {
  }

  create(request: AnswerCreationRequest): Observable<Answer> {
    return this.http.post<Answer>(`${URL_PREFIX}answers`, request);
  }

  getById(id: string): Observable<Answer> {
    return this.http.get<Answer>(`${URL_PREFIX}answers/${id}`);
  }
}
