import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Option, OptionCreationRequest} from '../../model/test/option-models';
import {Observable, of} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  constructor(private http: HttpClient) {
  }

  create(request: OptionCreationRequest): Observable<Option> {
    return this.http.post<Option>(`${URL_PREFIX}options`, request);
  }

  getById(id: number): Observable<Option> {
    return this.http.get<Option>(`${URL_PREFIX}options/${id}`);
  }
}
