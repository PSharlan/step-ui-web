import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Question, QuestionCreationRequest, QuestionPreview} from '../../model/test/question-models';
import {Observable, of} from 'rxjs';
import {Page} from '../../model/page-model';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})

export class QuestionService {
  constructor(private http: HttpClient) {
  }

  create(request: QuestionCreationRequest): Observable<Question> {
    return this.http.post<Question>(`${URL_PREFIX}questions`, request);
  }

  getAll(page: number, size: number): Observable<Page<QuestionPreview[]>> {
    return this.http.get<Page<QuestionPreview[]>>(`${URL_PREFIX}questions?page=${page}&size=${size}`);
  }

  getById(id: string): Observable<Question> {
    return this.http.get<Question>(`${URL_PREFIX}questions/${id}`);
  }
}
