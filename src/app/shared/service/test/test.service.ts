import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Test, TestCreationRequest, TestPreview} from '../../model/test/test-models';
import {Page} from '../../model/page-model';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})

export class TestService {
  constructor(private http: HttpClient) {
  }

  create(request: TestCreationRequest): Observable<Test> {
    return this.http.post<Test>(`${URL_PREFIX}tests`, request);
  }

  getAll(page: number, size: number): Observable<Page<TestPreview[]>> {
    return this.http.get<Page<TestPreview[]>>(`${URL_PREFIX}tests?page=${page}&size=${size}`);
  }

  geyById(id: number): Observable<Test> {
    return this.http.get<Test>(`${URL_PREFIX}tests/${id}`);
  }
}
