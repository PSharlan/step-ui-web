import {Injectable} from '@angular/core';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getTestModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  private questionTypes = {
    SELECT_ONE: 'Один из списка'
  };

  private questionLanguages = {
    JAVASCRIPT: 'JavaScript',
    PYTHON: 'Python',
    JAVA: 'Java',
    RUBY: 'Ruby',
    PHP: 'PHP',
    GO: 'Go',
    C: 'C'
  };

  private questionLevels = {
    EASY: 'Легкий',
    MEDIUM: 'Средний',
    HIGH: 'Сложный'
  };

  private testDirections = {
    BACKEND: 'Backend',
    FRONTEND: 'Frontend',
    FULLSTECK: 'Full-stack', // lol
    QA: 'QA',
    BUSINES_ANALYST: 'Business analyst' // lol x2
  };

  constructor() {
  }

  public getQuestionTypes(): any {
    return this.questionTypes;
  }

  public getQuestionLanguages(): any {
    return this.questionLanguages;
  }

  public getQuestionLevels(): any {
    return this.questionLevels;
  }

  public getTestLanguages(): any {
    return this.questionLanguages;
  }

  public getTestDirections(): any {
    return this.testDirections;
  }
}
