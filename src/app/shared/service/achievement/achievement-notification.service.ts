import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AchievementNotification} from '../../model/achievement/achievement-models';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAchievementModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class AchievementNotificationService {

  constructor(private http: HttpClient) {
  }

  getByUserId(id: number): Observable<AchievementNotification[]> {
    return this.http.get<AchievementNotification[]>(`${URL_PREFIX}achievementNotifications/users/${id}`);
  }

  deleteByAchievementId(id: number): Observable<void> {
    return this.http.delete<void>(`${URL_PREFIX}achievementNotifications/${id}`);
  }

  deleteByUserId(id: number): Observable<void> {
    return this.http.delete<void>(`${URL_PREFIX}achievementNotifications/users/${id}`);
  }
}
