import {Injectable} from '@angular/core';
import {URLPrefixService} from '../URL-prefix.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Player} from '../../model/achievement/player-models';

const URL_PREFIX = URLPrefixService.getAchievementModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) {
  }

  getByUserId(id: number): Observable<Player> {
    return this.http.get<Player>(`${URL_PREFIX}players/user/${id}`);
  }
}
