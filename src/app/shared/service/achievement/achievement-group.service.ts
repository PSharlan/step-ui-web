import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GroupAchievement, GroupAchievementCreationRequest} from '../../model/achievement/group-achievement-models';
import {Observable} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAchievementModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class AchievementGroupService {

  constructor(private http: HttpClient) {
  }

  create(request: GroupAchievementCreationRequest): Observable<GroupAchievement> {
    return this.http.post<GroupAchievement>(`${URL_PREFIX}groupAchievements/create`, request);
  }

  getAll(): Observable<GroupAchievement[]> {
    return this.http.get<GroupAchievement[]>(`${URL_PREFIX}groupAchievements`);
  }
}
