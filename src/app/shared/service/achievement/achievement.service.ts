import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {URLPrefixService} from '../URL-prefix.service';
import {Achievement, AchievementCreationRequest, AchievementPreview} from '../../model/achievement/achievement-models';
import {Observable} from 'rxjs';

const URL_PREFIX = URLPrefixService.getAchievementModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class AchievementService {

  constructor(private http: HttpClient) {
  }

  create(request: AchievementCreationRequest): Observable<Achievement> {
    return this.http.post<Achievement>(`${URL_PREFIX}achievements/create`, request);
  }

  getAll(): Observable<AchievementPreview[]> {
    return this.http.get<AchievementPreview[]>(`${URL_PREFIX}achievements`);
  }
}
