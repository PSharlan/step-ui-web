import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Road, RoadCreationRequest, RoadPreview, RoadUpdateRequest} from '../../model/roadmap/road-models';
import {Observable, of} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getRoadmapModuleUrlPrefix();

@Injectable ({
    providedIn: 'root'
})
export class RoadService {

    constructor(private http: HttpClient) {
    }

    create(request: RoadCreationRequest): Observable<Road> {
        // TODO: HTTP
        return this.generateOneRoad();
    }

    update(request: RoadUpdateRequest): Observable<Road> {
        // TODO: HTTP
        return this.generateOneRoad();
    }

    getById(id: number): Observable<Road> {
        // TODO: HTTP
        return this.generateOneRoad();
    }

    getAll(page: number, size: number): Observable<RoadPreview[]> {
        // TODO: HTTP
        return this.generateRoadPreviewsList();
    }

    private generateOneRoad(): Observable<Road> {
        return of(this.generateRoad());
    }

    private generateRoadPreviewsList(): Observable<RoadPreview[]> {

        const roadPreviews: RoadPreview[] = [
            this.generateRoad(),
            this.generateRoad(),
            this.generateRoad(),
            this.generateRoad(),
        ];

        return of(roadPreviews);
    }

    private generateRoad(): Road {

        const road: Road = {
            id: 1,
            title: 'New road',
            description: 'Description of new road',
            tags: [
                {
                    id: 20,
                    name: 'tag'
                }
            ],
            steps: [
                {
                    id: 10,
                    title: 'Step 1',
                    description: 'Descr of step 1',
                    roadId: 1,
                    position: 1,
                    resources: []
                },
                {
                    id: 11,
                    title: 'Step 2',
                    description: 'Descr of step 2',
                    roadId: 1,
                    position: 2,
                    resources: []
                },
                {
                    id: 12,
                    title: 'Step 3',
                    description: 'Descr of step 3',
                    roadId: 1,
                    position: 3,
                    resources: []
                },
                {
                    id: 13,
                    title: 'Step 4',
                    description: 'Descr of step 4',
                    roadId: 1,
                    position: 4,
                    resources: []
                }
            ]
        };

        return road;
    }
}
