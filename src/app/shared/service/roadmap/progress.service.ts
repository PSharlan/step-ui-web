import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Progress, ProgressCreationRequest, ProgressUpdateRequest} from '../../model/roadmap/progress-models';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getRoadmapModuleUrlPrefix();

@Injectable ({
    providedIn: 'root'
})
export class ProgressService {

    constructor(private http: HttpClient) {
    }

    create(request: ProgressCreationRequest): Observable<Progress> {
        // TODO: HTTP
        return this.generateOneProgress(-1, request.roadId);
    }

    update(request: ProgressUpdateRequest): Observable<Progress> {
        // TODO: HTTP
        return this.generateOneProgress(request.id, -1);
    }

    getById(progressId: number): Observable<Progress> {
        // TODO: HTTP
        return this.generateOneProgress(progressId, -1);
    }

    getByRoadId(roadId: number): Observable<Progress> {
        // TODO: HTTP
        return this.generateOneProgress(-1, roadId);
    }

    getAll(page: number, size: number): Observable<Progress[]> {
        // TODO: HTTP
        return this.generateProgressList();
    }

    private generateOneProgress(progressId: number, roadId: number): Observable<Progress> {
        return of(this.generateProgress(progressId, roadId));
    }

    private generateProgressList(): Observable<Progress[]> {

        const progressList: Progress[] = [
            this.generateProgress(-1, 0),
            this.generateProgress(-1, 1),
            this.generateProgress(-1, 2)
        ];

        return of(progressList);
    }

    private generateProgress(progressId: number, roadId: number): Progress {

        if (progressId < 0) {
            progressId = Math.trunc(Math.random() * 1000);
        }

        if (roadId < 0) {
            roadId = 0;
        }

        const progress: Progress = {
            id: progressId,
            road: {
                id: roadId,
                title: 'New road',
                description: 'Description of new road',
            },
            currentStep: {
                id: 12,
                title: 'Step 2',
                description: 'Descr of step 2',
                roadId: 0,
                position: 2,
                resources: []
            },
            lastActiveTime: 1609095850,
            userId: 0
        };

        return progress;
    }
}
