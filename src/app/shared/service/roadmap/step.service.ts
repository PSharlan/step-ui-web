import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Step, StepCreationRequest, StepUpdateRequest} from '../../model/roadmap/step-models';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getRoadmapModuleUrlPrefix();

@Injectable({
    providedIn: 'root'
})
export class StepService {

    constructor(private http: HttpClient) {
    }

    create(request: StepCreationRequest): Observable<Step> {
        return this.generateOneStep(0, request.roadId);
    }

    update(request: StepUpdateRequest): Observable<Step> {
        // TODO: HTTP
        return this.generateOneStep(request.id, 0);
    }

    getById(stepId: number): Observable<Step> {
        // TODO: HTTP
        return this.generateOneStep(stepId, 0);
    }

    getAll(roadId: number, page: number, size: number): Observable<Step[]> {
        // TODO: HTTP
        return this.generateStepList(roadId);
    }

    private generateOneStep(stepId: number, roadId: number): Observable<Step> {
        return of(this.generateStep(stepId, roadId));
    }

    private generateStepList(roadId: number): Observable<Step[]> {

        const stepList: Step[] = [
            this.generateStep(10, roadId),
            this.generateStep(11, roadId),
            this.generateStep(12, roadId),
            this.generateStep(13, roadId)
        ];

        return of(stepList);
    }

    private generateStep(stepId: number, roadId: number): Step {

        const step: Step = {
            id: stepId,
            title: 'Step ' + (stepId % 10),
            description: 'Descr of step ' + (stepId % 10),
            roadId,
            position: stepId % 10,
            resources: []
        };

        return step;
    }
}
