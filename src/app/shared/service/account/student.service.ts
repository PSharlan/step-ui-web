import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Student, StudentCreationRequest, StudentPreview} from '../../model/account/student-models';
import {Observable, of} from 'rxjs';
import {Page} from '../../model/page-model';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class StudentService {

    constructor(private http: HttpClient) {
    }

    create(request: StudentCreationRequest): Observable<Student> {
        return this.http.post<Student>(`${URL_PREFIX}students`, request);
    }

    getAll(page: number, size: number): Observable<Page<StudentPreview[]>> {
        return this.http.get<Page<StudentPreview[]>>(`${URL_PREFIX}students?pageNumber=${page}&pageSize=${size}`);
    }

    getById(id: number): Observable<Student> {
        return this.http.get<Student>(`${URL_PREFIX}students/${id}`);
    }
}
