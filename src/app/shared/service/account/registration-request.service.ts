import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Page} from '../../model/page-model';
import {RegistrationRequest} from '../../model/account/registration-request-models';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {URLPrefixService} from '../URL-prefix.service';
import {map} from 'rxjs/operators';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})

export class RegistrationRequestService {
  constructor(private http: HttpClient) {
  }

  getAll(page: number, size: number): Observable<Page<RegistrationRequest[]>> {
    return this.http.get<Page<RegistrationRequest[]>>(`${URL_PREFIX}registration/requests?pageNumber=${page}pageSize=${size}`);
  }

  getAllByGroupId(id: number): Observable<RegistrationRequest[]> {
    return this.http.get<RegistrationRequest[]>(`${URL_PREFIX}registration/requests/group/${id}`)
      .pipe(map(students => students.filter(student => student.status === 'PENDING')));
  }

  confirmById(id: number): Observable<RegistrationRequest> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<RegistrationRequest>(`${URL_PREFIX}registration/requests/${id}/confirm`, {headers});
  }
}
