import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Teacher, TeacherCreationRequest, TeacherPreview} from '../../model/account/teacher-models';
import {Observable, of} from 'rxjs';
import {Page} from '../../model/page-model';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

    constructor(private http: HttpClient) {
    }

    create(request: TeacherCreationRequest): Observable<Teacher> {
        return this.http.post<Teacher>(`${URL_PREFIX}teachers`, request);
    }

    getAll(page: number, size: number): Observable<Page<TeacherPreview[]>> {
        return this.http.get<Page<TeacherPreview[]>>(`${URL_PREFIX}teachers?pageNumber=${page}&pageSize=${size}`);
    }

    getById(id: number): Observable<Teacher> {
        return this.http.get<Teacher>(`${URL_PREFIX}teachers/${id}`);
    }
}
