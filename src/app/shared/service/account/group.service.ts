import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Group, GroupCreationRequest, GroupPreview} from '../../model/account/group-models';
import {Observable} from 'rxjs';
import {Page} from '../../model/page-model';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private http: HttpClient) {
  }

  create(request: GroupCreationRequest): Observable<Group> {
    return this.http.post<Group>(`${URL_PREFIX}groups`, request);
  }

  getAll(page: number, size: number): Observable<Page<GroupPreview[]>> {
    return this.http.get<Page<GroupPreview[]>>(`${URL_PREFIX}groups?pageNumber=${page}&pageSize=${size}`);
  }

  getById(id: number): Observable<Group> {
    return this.http.get<Group>(`${URL_PREFIX}groups/${id}`);
  }

  getByName(name: string): Observable<Group> {
    return this.http.get<Group>(`${URL_PREFIX}groups/name/${name}`);
  }

  getAllByUserId(id: number): Observable<GroupPreview[]> {
    return this.http.get<GroupPreview[]>(`${URL_PREFIX}groups/users/${id}`);
  }
}
