import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Admin} from '../../model/account/admin-models';
import {Observable, of} from 'rxjs';
import {URLPrefixService} from '../URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();

@Injectable({
  providedIn: 'root'
})
export class StudentService {

    constructor(private http: HttpClient) {
    }
}
