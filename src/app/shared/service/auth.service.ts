import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LoginRequest} from '../model/login-request-model';
import {HttpClient} from '@angular/common/http';
import {AuthenticationResponse} from '../model/authentication-response-models';
import {URLPrefixService} from './URL-prefix.service';

const URL_PREFIX = URLPrefixService.getAccountModuleUrlPrefix();
const ACCESS_TOKEN_KEY = 'access';
const LOGINED_USER_ID = 'id';
const LOGINED_USER_ROLE = 'role';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(request: LoginRequest): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(`${URL_PREFIX}login`, request);
  }

  saveToken(response: AuthenticationResponse): void {
    sessionStorage.setItem(ACCESS_TOKEN_KEY, response.token);
  }

  saveUserId(response: AuthenticationResponse): void {
    sessionStorage.setItem(LOGINED_USER_ID, response.id.toString());
  }

  saveUserRole(response: AuthenticationResponse): void {
    sessionStorage.setItem(LOGINED_USER_ROLE, response.userRole);
  }

  getToken(): string {
    return sessionStorage.getItem(ACCESS_TOKEN_KEY) as string;
  }

  getUserId(): number {
    const userId = sessionStorage.getItem(LOGINED_USER_ID) as string;
    return +userId;
  }

  getUserRole(): string {
    return sessionStorage.getItem(LOGINED_USER_ROLE) as string;
  }

  isAuthenticated(): boolean {
    return (sessionStorage.getItem(ACCESS_TOKEN_KEY) && sessionStorage.getItem(LOGINED_USER_ROLE)) !== null;
  }

  logout(): void {
    sessionStorage.clear();
  }

}
