import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable({providedIn: 'root'})
export class URLPrefixService {

  private static TEST_MODULE_URL_PREFIX = environment.testModuleUrl;
  private static ROADMAP_MODULE_URL_PREFIX = environment.roadmapModuleUrl;
  private static ACCOUNT_MODULE_URL_PREFIX = environment.accountModuleUrl;
  private static HOMEWORK_MODULE_URL_PREFIX = environment.homeworkModuleUrl;
  private static ACHIEVEMENT_MODULE_URL_PREFIX = environment.achievementModuleUrl;

  public static getTestModuleUrlPrefix(): string {
    return this.TEST_MODULE_URL_PREFIX;
  }

  public static getRoadmapModuleUrlPrefix(): string {
    return this.ROADMAP_MODULE_URL_PREFIX;
  }

  public static getAccountModuleUrlPrefix(): string {
    return this.ACCOUNT_MODULE_URL_PREFIX;
  }

  public static getHomeworkModuleUrlPrefix(): string {
    return this.HOMEWORK_MODULE_URL_PREFIX;
  }

  public static getAchievementModuleUrlPrefix(): string {
    return this.ACHIEVEMENT_MODULE_URL_PREFIX;
  }
}
