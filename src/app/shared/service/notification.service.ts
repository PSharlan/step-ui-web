import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {NotificationConfig, NotificationContent} from '../model/notification-dialog-models';
import {NotificationDialogComponent} from '../components/notification-dialog/notification-dialog.component';

const DEFAULT_DURATION_MS = 5000;
const DEFAULT_VERTICAL_POSITION = 'top';
const DEFAULT_BACKGROUND_COLOR = '';
const ERROR_BACKGROUND_COLOR = 'error-notification';

@Injectable({providedIn: 'root'})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) {
  }

  public showDefaultNotification(header: string): void {
    const config = this.getDefaultNotificationConfig(header);
    this.snackBar.openFromComponent(NotificationDialogComponent, config);
  }

  public showErrorNotification(header: string): void {
    const config = this.getErrorNotificationConfig(header);
    this.snackBar.openFromComponent(NotificationDialogComponent, config);
  }

  private getNotificationConfig(data: NotificationContent,
                                duration: number,
                                verticalPosition: MatSnackBarVerticalPosition,
                                panelClass: string): NotificationConfig {
    return {data, duration, verticalPosition, panelClass};
  }

  private getDefaultNotificationConfig(header: string): NotificationConfig {
    const data = {header};
    return this.getNotificationConfig(data, DEFAULT_DURATION_MS, DEFAULT_VERTICAL_POSITION, DEFAULT_BACKGROUND_COLOR);
  }

  private getErrorNotificationConfig(header: string): NotificationConfig {
    const data = {header};
    return this.getNotificationConfig(data, DEFAULT_DURATION_MS, DEFAULT_VERTICAL_POSITION, ERROR_BACKGROUND_COLOR);
  }

}
