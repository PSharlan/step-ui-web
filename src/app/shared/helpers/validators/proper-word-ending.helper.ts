
// works not for all words obviously
export function getProperEnding(word: string, quantity: number): string {

    const lastDigit = quantity % 10;
    if (10 <= quantity % 100 && quantity % 100 <= 20) {
        word += 'ов';
    }
    else {
        if (lastDigit === 0 || lastDigit >= 5) {
            word += 'ов';
        }
        else if (lastDigit >= 2 && lastDigit <= 4) {
            word += 'а';
        }
    }

    return word;
}
