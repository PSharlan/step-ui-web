import {delay} from 'rxjs/operators';
import {LoadingService} from '../service/loading.service';

export abstract class AsyncLoadable {
    private loading!: boolean;
    private loaderDisplayTimeout = 500;
    public displayLoader!: boolean;

    constructor(private loadingService: LoadingService) {
        this.listenToLoading();
    }

    listenToLoading(): void {
        this.loadingService.loadingSub
          .pipe(delay(0)) // This prevents a ExpressionChangedAfterItHasBeenCheckedError for subsequent requests
          .subscribe((loading: boolean) => {
            this.loading = loading;
            setTimeout(() => {
                if (this.getLoading()) {
                  this.displayLoader = true;
                }
            }, this.loaderDisplayTimeout);
            if (!loading) {
              this.displayLoader = false;
            }
          });
      }

    private getLoading(): boolean {
        return this.loading;
    }
}
