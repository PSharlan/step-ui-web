import {ImageHelper} from './image.helper';

export class FilesHelper {

  private files: FileInput;

  public constructor() {
    this.files = {
      fileList: [],
      fileThumbs: []
    };
  }

  public getFiles(): FileInput {
    return this.files;
  }

  public handleFileInput(files: FileList): FileInput {
    // TODO: REMOVE WHEN FILE UPLOADING IS IMPLEMENTED
    alert('[WARNING] File(s) will not be uploaded (file uploading is not implemented)');

    if (this.files) {
      this.files.fileList = this.convertFileListToArray(files);
      this.files.fileThumbs = ImageHelper.getThumbnails(files);
    }
    else {
      this.files = {
        fileList: this.convertFileListToArray(files),
        fileThumbs: ImageHelper.getThumbnails(files)
      };
    }

    return this.files;
  }

  public deleteFile(index: number): FileInput {
    if (this.files && 0 <= index && index < this.files.fileList.length) {
      this.files.fileList.splice(index, 1);
      this.files.fileThumbs.splice(index, 1);
    }

    return this.files;
  }

  private convertFileListToArray(fileList: FileList): File[] {
    const output: File[] = [];
    for (let i = 0; i < fileList.length; i++) {
      const file = fileList.item(i);
      if (file !== null) {
        output.push(file);
      }
    }

    return output;
  }
}

interface FileInput {
  fileList: File[];
  fileThumbs: string[];
}
