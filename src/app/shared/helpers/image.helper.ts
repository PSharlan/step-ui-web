export class ImageHelper {

  public static getThumbnail(file: File): string {
    const extension = file.name.split('.').pop();

    let output = '/thumbnails/';

    switch (extension) {
      case 'jpg':
      case 'jpeg':
      case 'png':
        output += 'image-default.svg';
        break;
      case 'txt':
        output += 'txt-default.svg';
        break;
      default:
        output += 'file-default.svg';
        break;
    }

    return output;
  }

  public static getThumbnails(fileList: FileList): string[] {
    const output: string[] = [];

    // cause fileList doesn't have Iterator
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < fileList.length; i++) {
      output.push(this.getThumbnail(fileList[i]));
    }

    return output;
  }
}
