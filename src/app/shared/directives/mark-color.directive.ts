import {AfterViewInit, Directive, ElementRef, HostBinding, Input} from '@angular/core';

const COLOR_GREEN = '#2AB930';
const COLOR_ORANGE = '#ED9526';
const COLOR_RED = '#B92A2A';

@Directive({
  selector: '[appMarkColor]'
})
export class MarkColorDirective implements AfterViewInit {
  constructor(private elementRef: ElementRef) {
  }

  ngAfterViewInit(): void {
    const mark = this.elementRef.nativeElement.innerText;
    this.elementRef.nativeElement.style.color = this.changeColor(mark);
  }

  changeColor(mark: number): string {
    let newColor;
    if (0 <= mark && mark <= 4) {
      newColor = COLOR_RED;
    } else if (5 <= mark && mark <= 7) {
      newColor = COLOR_ORANGE;
    } else {
      newColor = COLOR_GREEN;
    }
    return newColor;
  }
}
