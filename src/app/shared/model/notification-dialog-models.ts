import {MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

export interface NotificationContent {
  header: string;
}

export interface NotificationConfig {
  data: NotificationContent;
  duration: number;
  verticalPosition: MatSnackBarVerticalPosition;
  panelClass: string;
}
