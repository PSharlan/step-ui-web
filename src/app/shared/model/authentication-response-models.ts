export interface AuthenticationResponse {
  email: string;
  id: number;
  token: string;
  userRole: string;
}
