export interface OptionCreationRequest {
  isCorrect: boolean;
  text: string;
}

export interface Option {
  id: number;
  text: string;
}
