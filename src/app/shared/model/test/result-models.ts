import {Answer, AnswerCreationRequest} from './answer-models';
import {TestPreview} from './test-models';

export interface ResultCreationRequest {
  testId: number;
  userId: number;
  timeSpent: number;
  answers: AnswerCreationRequest[];
}

export interface Result {
  id: number;
  testPreview: TestPreview;
  userId: number;
  timeSpent: number;
  score: number;
  answers: Answer[];
}

export interface ResultPreview {
  id: number;
  testPreview: TestPreview;
  userId: number;
  score: number;
}
