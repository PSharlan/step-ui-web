export interface AnswerCreationRequest {
  questionId: number;
  optionId: number;
}

export interface Answer {
  id: number;
  questionId: number;
  optionId: number;
  resultId: number;
  isCorrect: boolean;
}

