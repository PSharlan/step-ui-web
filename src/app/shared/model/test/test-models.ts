import {Question} from './question-models';

export interface TestCreationRequest {
  authorId: number;
  direction: string;
  language: string;
  level: string;
  name: string;
  questionIds: number[];
}

export interface Test {
  id: number;
  name: string;
  language: string;
  direction: string;
  level: string;
  rating: number;
  authorId: number;
  questions: Question[];
}

export interface TestPreview {
  id: number;
  name: string;
  language: string;
  direction: string;
  level: string;
  rating: number;
  authorId: number;
}

export interface TestUpdateRequest {
  id: number;
  name: string;
  direction: string;
  level: string;
  rating: number;
  authorId: number;
  questionIds: number[];
}
