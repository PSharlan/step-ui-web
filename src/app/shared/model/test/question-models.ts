import {Option, OptionCreationRequest} from './option-models';

export interface QuestionCreationRequest {
  authorId: number;
  codeBlock: string;
  language: string;
  level: string;
  options: OptionCreationRequest[];
  text: string;
  type: string;
}

export interface Question {
  authorId: number;
  codeBlock: string;
  id: number;
  language: string;
  level: string;
  options: Option[];
  status: string;
  text: string;
  type: string;
}

export interface QuestionPreview {
  id: number;
  text: string;
  level: string;
  language: string;
  type: string;
}

export interface QuestionUpdateRequest {
  id: number;
  text: string;
  authorId: number;
  status: string;
  options: OptionCreationRequest[];
}
