import {Resource, ResourceCreationRequest} from './resource-models';

export interface StepCreationRequest {
    title: string;
    description: string;
    roadId: number;
    position: number;
    resources: ResourceCreationRequest[];
}

export interface Step {
    id: number;
    title: string;
    description: string;
    roadId: number;
    position: number;
    resources: Resource[];
}

export interface StepUpdateRequest {
    id: number;
    title: string;
    description: string;
    position: number;
}
