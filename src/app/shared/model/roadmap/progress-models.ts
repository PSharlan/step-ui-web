import {RoadPreview} from './road-models';
import {Step} from './step-models';

export interface ProgressCreationRequest {
    roadId: number;
    stepId: number;
    userId: number;
}

export interface Progress {
    id: number;
    road: RoadPreview;
    currentStep: Step;
    lastActiveTime: number;
    userId: number;
}

export interface ProgressUpdateRequest {
    id: number;
    currentStepId: number;
    lastActiveTime: number;
}
