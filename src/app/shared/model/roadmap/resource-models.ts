export interface ResourceCreationRequest {
    resourceUrl: string;
    resourceType: string;
}

export interface Resource {
    id: number;
    stepId: number;
    resourceUrl: string;
    resourceType: string;
}

export interface ResourceUpdateRequest {
    id: number;
    resourceUrl: string;
    resourceType: string;
}
