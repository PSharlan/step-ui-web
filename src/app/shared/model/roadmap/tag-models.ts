export interface TagCreationRequest {
    name: string;
}

export interface TagPreview {
    id: number;
    name: string;
}

export interface TagUpdateRequest {
    id: number;
    name: string;
}
