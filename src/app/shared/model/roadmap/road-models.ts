import {Step} from './step-models';
import {TagPreview} from './tag-models';

export interface RoadCreationRequest {
    title: string;
    description: string;
    tagIds: number[];
}

export interface Road {
    id: number;
    title: string;
    description: string;
    tags: TagPreview[];
    steps: Step[];
}

export interface RoadPreview {
    id: number;
    title: string;
    description: string;
}

export interface RoadUpdateRequest {
    id: number;
    title: string;
    description: string;
}
