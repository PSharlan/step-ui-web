import {GroupPreview} from './group-models';

export interface TeacherCreationRequest {
  name: string;
  lastName: string;
  email: string;
  password: string;
}

export interface Teacher {
  id: number;
  name: string;
  lastName: string;
  email: string;
  password: string;
  registrationDate: string;
  lastLoginDate: string;
  userRole: string;
  teachingGroups: GroupPreview[];
}

export interface TeacherPreview {
  id: number;
  name: string;
  lastName: string;
}

export interface TeacherUpdateRequest {
  id: number;
  name: string;
  lastName: string;
  email: string;
  password: string;
  userRole: string;
}
