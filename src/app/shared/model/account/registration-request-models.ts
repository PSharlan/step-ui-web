export interface RegistrationRequest {
  id: number;
  status: string;
  userEmail: string;
  userId: number;
  userName: string;
  userLastName: string;
}
