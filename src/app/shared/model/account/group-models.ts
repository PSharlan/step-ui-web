import {StudentPreview} from './student-models';
import {TeacherPreview} from './teacher-models';

export interface GroupCreationRequest {
  subjectOfStudy: string;
  startDate: string;
  trainingPeriod: string;
}

export interface Group {
  id: number;
  subjectOfStudy: string;
  name: string;
  startDate: string;
  trainingPeriod: string;
  students: StudentPreview[];
  teacher: TeacherPreview;
}

export interface GroupPreview {
  id: number;
  subjectOfStudy: string;
  name: string;
  startDate: string;
  teacher: TeacherPreview;
}

export interface GroupUpdate {
  id: number;
  subjectOfStudy: string;
  startDate: string;
  trainingPeriod: string;
  teacherId: number;
}
