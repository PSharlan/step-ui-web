import {GroupPreview} from './group-models';
import {AchievementPreview} from '../achievement/achievement-models';

export interface StudentCreationRequest {
  name: string;
  lastName: string;
  email: string;
  password: string;
  groupId: number;
  rating: number;
}

export interface Student {
  id: number;
  name: string;
  lastName: string;
  email: string;
  registrationDate: string;
  lastLoginDate: string;
  group: GroupPreview;
  userRole: string;
  rating: number;
}

export interface StudentPreview {
  id: number;
  name: string;
  lastName: string;
  rating: number;
  email: string;
}

export interface StudentPreviewWithAchievements {
  student: StudentPreview;
  achievements: AchievementPreview[];
}

export interface StudentUpdateRequest {
  id: number;
  name: string;
  lastName: string;
  password: string;
  groupId: number;
  userRole: string;
}
