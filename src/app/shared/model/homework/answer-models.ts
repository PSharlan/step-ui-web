export interface Answer {
  answerResourceUrl: string;
  answerStatus: string;
  created: string;
  homeworkId: number;
  id: number;
  realGrade: number;
  studentId: number;
  teacherComment: string;
  studentComment: string;
}

export interface AnswerCreationRequest {
  answerResourceUrl: string;
  homeworkId: number;
  studentId: number;
  studentComment: string;
}

export interface AnswerUpdateRequest {
  answerResourceUrl: string;
  answerStatus: string;
  id: number;
  realGrade: number;
  teacherComment: string;
}
