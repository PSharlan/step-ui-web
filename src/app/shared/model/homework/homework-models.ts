import {Answer} from './answer-models';

export interface Homework {
  created: string;
  lessonDate: string;
  deadLine: string;
  groupId: number;
  id: number;
  teacherId: number;
  topic: string;
  description: string;
  fileUrl: string;
  answers: Answer[];
}

export interface HomeworkCreationRequest {
  deadLine: string;
  lessonDate: string;
  description: string;
  groupId: number;
  teacherId: number;
  topic: string;
  taskResourceUrl: string;
}

export interface HomeworkPreview {
  created: string;
  deadLine: string;
  groupId: number;
  id: number;
  description: string;
  teacherId: number;
  topic: string;
  realGrade: number;
  answerStatus: string;
}

export interface HomeworkWithAnswerPreview {
  created: string;
  deadLine: string;
  lessonDate: string;
  groupId: number;
  id: number;
  teacherId: number;
  topic: string;
  realGrade: number;
  answerStatus: string;
  description: string;
  taskResourceUrl: string;
}

export interface HomeworkUpdateRequest {
  deadLine: string;
  description: string;
  groupId: number;
  id: number;
  topic: string;
}
