import {Achievement} from './achievement-models';

export interface GroupAchievementCreationRequest {
  backgroundImg: string;
  groupName: string;
}

export interface GroupAchievement {
  achievements: Achievement[];
  backgroundImg: string;
  groupName: string;
  id: number;
}

export interface GroupAchievementUpdateRequest {
  backgroundImg: string;
  groupName: string;
  id: number;
}

