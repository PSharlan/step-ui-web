import {PlayerPreview} from './player-models';

export const actions = [
  'TEST_CREATING',
  'TEST_PASSING',
  'CREATING_OF_THE_ROAD',
  'BEGINNING_OF_THE_ROAD',
  'MIDDLE_OF_THE_ROAD',
  'END_OF_THE_ROAD',
  'FOUND_BUG',
  'BECAME_A_MENTOR',
  'BEGINNING_OF_THE_STUDYING_PROCESS',
  'END_OF_THE_STUDYING_PROCESS'
];

export const finishGroupAction = 'FINISH_GROUP';

export interface AchievementCreationRequest {
  action: string;
  groupId: number;
  img: string;
  level: number;
  threshold: number;
  typeOfAchievement: string;
}

export interface Achievement {
  action: string;
  groupId: number;
  id: number;
  img: string;
  level: number;
  players: PlayerPreview[];
  threshold: number;
  typeOfAchievement: string;
}

export interface AchievementPreviewDto {
  action: string;
  id: number;
  img: string;
  level: number;
  threshold: number;
  typeOfAchievement: string;
}

export interface PlayerPreviewDto {
  playerRank: string;
  userId: number;
}

export interface AchievementNotification {
  achievementPreviewDto: AchievementPreview;
  id: number;
  playerPreviewDto: PlayerPreview;
}

export interface AchievementPreview {
  action: string;
  id: number;
  img: string;
  level: number;
  threshold: number;
  typeOfAchievement: string;
}

export interface AchievementUpdateRequest {
  action: string;
  groupId: number;
  id: number;
  img: string;
  level: number;
  threshold: number;
  typeOfAchievement: string;
}
