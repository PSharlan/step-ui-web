import {Achievement} from './achievement-models';

export interface PlayerCommittedAction {
  action: string;
  userId: number;
}

export interface PlayerCreationRequest {
  userId: number;
}

export interface Player {
  achievements: Achievement[];
  id: number;
  playerRank: string;
  playersProgress: PlayerProgress[];
  userId: number;
}

export interface PlayerPreview {
  playerRank: string;
  userId: number;
}

export interface PlayerProgress {
  action: string;
  counterAction: number;
  id: number;
  playerId: number;
}
