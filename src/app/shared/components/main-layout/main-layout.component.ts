import {Component, OnDestroy, OnInit} from '@angular/core';
import {GroupService} from '../../service/account/group.service';
import {SubSink} from 'subsink';
import {GroupPreview} from '../../model/account/group-models';
import {AuthService} from '../../service/auth.service';
import {AsyncLoadable} from '../../helpers/loader-listener.helper';
import {LoadingService} from '../../service/loading.service';
import {Router} from '@angular/router';
import {AchievementNotificationService} from '../../service/achievement/achievement-notification.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AchievementNotificationDialogComponent} from '../achievement-notification-dialog/achievement-notification-dialog.component';
import {switchMap} from 'rxjs/operators';
import {interval} from 'rxjs';
import {PlayerService} from '../../service/achievement/player.service';
import {ErrorHandlerService} from '../../service/error-handler.service';
import {AchievementNotification, AchievementPreview} from '../../model/achievement/achievement-models';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent extends AsyncLoadable implements OnInit, OnDestroy {

  public groups: GroupPreview[] = [];
  public userAchievements: AchievementPreview[] = [];
  private subs = new SubSink();
  private dialogRef!: MatDialogRef<AchievementNotificationDialogComponent>;

  constructor(private groupService: GroupService,
              private authService: AuthService,
              private router: Router,
              private achievementNotificationService: AchievementNotificationService,
              private playerService: PlayerService,
              private dialog: MatDialog,
              private errorHandlerService: ErrorHandlerService,
              loadingService: LoadingService) {
    super(loadingService);
  }

  ngOnInit(): void {
    this.loadGroups();
    this.loadAchievementNotifications();
    this.getAchievements();
  }

  public getAchievements(): void {
    this.subs.sink = this.playerService.getByUserId(this.authService.getUserId()).subscribe(player => {
      this.userAchievements = player.achievements;
    }, error => {
      this.errorHandlerService.handleError(error);
    });
  }

  private loadGroups(): void {
    const userId = this.authService.getUserId();
    this.subs.sink = this.groupService.getAllByUserId(userId).subscribe(groups => {
      this.groups = groups;
    });
  }

  private loadAchievementNotifications(): void {
    const userId = this.authService.getUserId();
    const timeOut = interval(5000);
    this.subs.sink = timeOut.pipe(
      switchMap(() => this.achievementNotificationService.getByUserId(userId)))
      .subscribe((notifications: AchievementNotification[]) => {
        if (notifications.length > 0 && (!this.dialog.openDialogs || !this.dialog.openDialogs.length)) {
          this.dialogRef = this.dialog.open(AchievementNotificationDialogComponent, {
            data: {userNotifications: notifications},
            panelClass: 'achievement-notification-dialog'
          });
          this.subs.sink = this.dialogRef.afterClosed().pipe(
            switchMap(() => {
              return this.achievementNotificationService.deleteByUserId(this.authService.getUserId());
            })
          ).subscribe();
        }
      });
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/account']);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
