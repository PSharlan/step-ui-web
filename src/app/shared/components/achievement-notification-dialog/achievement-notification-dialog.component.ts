import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AchievementNotification} from '../../model/achievement/achievement-models';
import {AchievementNotificationService} from '../../service/achievement/achievement-notification.service';
import {SubSink} from 'subsink';

@Component({
  selector: 'app-achievement-notification-dialog',
  templateUrl: './achievement-notification-dialog.component.html',
  styleUrls: ['./achievement-notification-dialog.component.css']
})
export class AchievementNotificationDialogComponent implements OnInit, OnDestroy {

  public notifications: AchievementNotification[] = [];
  private subs = new SubSink();

  constructor(private achievementNotificationService: AchievementNotificationService,
              public dialogRef: MatDialogRef<AchievementNotificationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { userNotifications: AchievementNotification[] }) {
  }

  ngOnInit(): void {
    this.notifications = this.data.userNotifications;
    this.confirmCurrentNotification();
  }

  closeDialog(): void {
    this.deleteCurrentNotification();
    if (this.notifications.length === 0) {
      this.dialogRef.close();
      return;
    }
    this.confirmCurrentNotification();
  }

  private confirmCurrentNotification(): void {
    this.subs.sink = this.achievementNotificationService.deleteByAchievementId(this.notifications[0].id)
      .subscribe();
  }

  private deleteCurrentNotification(): void {
    this.notifications.splice(0, 1);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
