import {Component, OnInit} from '@angular/core';
import {delay} from 'rxjs/operators';
import {AsyncLoadable} from '../../helpers/loader-listener.helper';
import {LoadingService} from '../../service/loading.service';

@Component({
  selector: 'app-registration-layout',
  templateUrl: './registration-layout.component.html',
  styleUrls: ['./registration-layout.component.css']
})
export class RegistrationLayoutComponent extends AsyncLoadable implements OnInit {

  constructor(loadingService: LoadingService) {
    super(loadingService);
  }

  ngOnInit(): void {
    this.listenToLoading();
  }
}
