import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {Progress} from 'src/app/shared/model/roadmap/progress-models';
import {Road} from 'src/app/shared/model/roadmap/road-models';
import {RoadService} from 'src/app/shared/service/roadmap/road.service';
import {ProgressService} from 'src/app/shared/service/roadmap/progress.service';
import {SubSink} from 'subsink';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-single-roadmap',
  templateUrl: './single-roadmap.component.html',
  styleUrls: ['./single-roadmap.component.css']
})
export class SingleRoadmapComponent implements OnInit, OnDestroy {

  public roadmap!: Road;
  public progress!: Progress;
  public progressPercentage!: number;

  private subs = new SubSink();

  constructor(private route: ActivatedRoute,
              private roadService: RoadService,
              private progressService: ProgressService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.loadRoadmap();
    this.loadProgress();
  }

  ngOnDestroy(): void {
    this.subs.sink?.unsubscribe();
  }

  private loadRoadmap(): void {
    this.route.params.pipe(
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
      switchMap((params: Params) => {
        return this.roadService.getById(params.id);
      }),
      tap((road: Road) => {
        this.roadmap = road;
      })).subscribe();
  }

  private loadProgress(): void {
    this.subs.sink = this.progressService.getByRoadId(this.roadmap.id).subscribe(progress => {
      this.progress = progress;
    }, error => {
      this.errorHandlerService.handleError(error);
    });

    this.countProgressPercentage();
  }

  private countProgressPercentage(): void {
    this.progressPercentage = Math.trunc((this.progress.currentStep.position - 1) * 100 / this.roadmap.steps.length);
  }
}
