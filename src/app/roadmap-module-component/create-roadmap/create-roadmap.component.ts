import {Component, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {RoadService} from 'src/app/shared/service/roadmap/road.service';

@Component({
  selector: 'app-create-roadmap',
  templateUrl: './create-roadmap.component.html',
  styleUrls: ['./create-roadmap.component.css']
})
export class CreateRoadmapComponent {

  @Input()
  public roadMapTitle!: string;
  @Input()
  public roadMapCategory!: string;
  @Input()
  public newStepTitle!: string;
  @Input()
  public newStepDescription!: string;
  @Input()
  public editingStepId = -1;

  public steps: any[] = [];

  constructor(private roadService: RoadService) { }

  public addStep(): void {
    const step = {
      tempId: this.steps.length,
      title: this.newStepTitle,
      description: this.newStepDescription
    };

    this.steps.push(step);

    this.clearStepInputs();
  }

  public prepareForEditing(tempId: number): void {
    this.editingStepId = tempId;
    const step = this.getStepByTempID(this.editingStepId);

    this.newStepTitle = step.title;
    this.newStepDescription = step.description;
  }

  public saveEditedStep(): void {
    const step = this.getStepByTempID(this.editingStepId);

    step.title = this.newStepTitle;
    step.description = this.newStepDescription;
    this.editingStepId = -1;
    this.clearStepInputs();
  }

  public deleteStep(tempId: number): void {
    this.steps = this.steps.filter(step => step.tempId !== tempId);

    if (this.editingStepId === tempId) {
      this.editingStepId = -1;
      this.clearStepInputs();
    }
  }

  public saveRoadmap(): void {
    // TODO: saving logic
    // save roadmap, get new id
    // form array of steps, save them
  }

  public checkStepInputs(): boolean {
    return !!this.newStepTitle && !!this.newStepDescription;
  }

  public checkRoadmapInputs(): boolean {
    return !!this.roadMapTitle && !!this.roadMapCategory && this.steps.length > 0;
  }

  public stepDropped(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.steps, event.previousIndex, event.currentIndex);
  }

  private clearStepInputs(): void {
    this.newStepTitle = '';
    this.newStepDescription = '';
    this.editingStepId = -1;
  }

  private getStepByTempID(tempId: number): any {
    return this.steps.filter(step => step.tempId === tempId)[0];
  }
}
