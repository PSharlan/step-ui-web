import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ModuleMenuComponent} from './module-menu/module-menu.component';
import {StarredComponent} from './starred/starred.component';
import {MyRoadmapsComponent} from './my-roadmaps/my-roadmaps.component';
import {AppMaterialModule} from '../app-material.module';
import {CreateRoadmapComponent} from './create-roadmap/create-roadmap.component';
import {SingleRoadmapComponent} from './single-roadmap/single-roadmap.component';

@NgModule({
  declarations: [
    ModuleMenuComponent,
    StarredComponent,
    MyRoadmapsComponent,
    CreateRoadmapComponent,
    SingleRoadmapComponent
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '', component: ModuleMenuComponent, children: [
          {path: '', redirectTo: '/roadmap-module/starred', pathMatch: 'full'},
          {path: 'starred', component: StarredComponent},
          {path: 'roadmaps', component: MyRoadmapsComponent},
          {path: 'roadmaps/:id', component: SingleRoadmapComponent},
          {path: 'starred/:id', component: SingleRoadmapComponent}
        ]
      }
    ]),
    CommonModule,
    AppMaterialModule
  ],
  exports: [RouterModule]
})

export class RoadmapModule {
}
