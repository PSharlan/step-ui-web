import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubSink} from 'subsink';
import {RoadPreview} from '../../shared/model/roadmap/road-models';
import {RoadService} from '../../shared/service/roadmap/road.service';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';

@Component({
  selector: 'app-my-roadmaps',
  templateUrl: './my-roadmaps.component.html',
  styleUrls: ['./my-roadmaps.component.css']
})
export class MyRoadmapsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  roads: RoadPreview[] = [];
  currentPage = 0;
  pageLimit = 0;

  constructor(private roadService: RoadService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.loadRoads();
  }

  private loadRoads(): void {
    this.subs.sink = this.roadService.getAll(this.currentPage, this.pageLimit).subscribe(roads => {
      this.roads = roads;
      console.log(this.roads);
    }, error => {
      this.errorHandlerService.handleError(error);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
