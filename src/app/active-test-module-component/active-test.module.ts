import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActiveTestComponent} from './active-test/active-test.component';
import {ResultTestComponent} from './result-test/result-test.component';
import {RouterModule} from '@angular/router';
import {AppMaterialModule} from '../app-material.module';

@NgModule({
  declarations: [
    ActiveTestComponent,
    ResultTestComponent
  ],
  imports: [
    RouterModule.forChild([
      {path: 'test/:id', component: ActiveTestComponent},
      {path: 'result/:id', component: ResultTestComponent}
    ]),
    CommonModule,
    AppMaterialModule
  ]
})

export class ActiveTestModule {
}
