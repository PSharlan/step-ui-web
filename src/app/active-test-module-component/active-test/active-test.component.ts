import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TestService} from '../../shared/service/test/test.service';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {Test} from '../../shared/model/test/test-models';
import {ResultService} from '../../shared/service/test/result.service';
import {ResultCreationRequest} from '../../shared/model/test/result-models';
import {AnswerCreationRequest} from '../../shared/model/test/answer-models';
import {AuthService} from '../../shared/service/auth.service';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {throwError} from 'rxjs';
import * as hljs from 'highlight.js';
import {SubSink} from 'subsink';

@Component({
  selector: 'app-active-test',
  templateUrl: './active-test.component.html',
  styleUrls: ['./active-test.component.css']
})
export class ActiveTestComponent implements OnInit, OnDestroy {
  public test!: Test;
  public currentQuestion: number;
  private userAnswers: AnswerCreationRequest[];
  private startDate = new Date();
  private userId: number;
  private subs = new SubSink();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private testService: TestService,
              private resultService: ResultService,
              private authService: AuthService,
              private errorHandlerService: ErrorHandlerService) {
    this.currentQuestion = 0;
    this.userAnswers = [];
    this.userId = 0;
  }

  ngOnInit(): void {
    this.subs.sink = this.route.params.pipe(
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
      switchMap((params: Params) => {
        return this.testService.geyById(params.id);
      }),
      tap((test: Test) => {
        this.test = test;
        this.colorizeCode();
      })).subscribe();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  public handleAnswer(questionId: number, optionId: number): void {
    this.userAnswers.push({questionId, optionId});
    if (!this.checkLastAnswer()) {
      this.currentQuestion++;
    }
  }

  private colorizeCode(): void {
    for (const question of this.test.questions) {
      if (question.codeBlock) {
        question.codeBlock = hljs.highlight(question.language, question.codeBlock).value;
      }
    }
  }

  private checkLastAnswer(): boolean {
    if (this.currentQuestion + 1 === this.test.questions.length) {
      console.log('test finished');
      this.sendUserAnswers();
      return true;
    }
    return false;
  }

  private sendUserAnswers(): void {
    const timePassed = (new Date()).getTime() - this.startDate.getTime();
    const request: ResultCreationRequest = {
      testId: this.test.id,
      userId: this.authService.getUserId(),
      timeSpent: timePassed,
      answers: this.userAnswers
    };
    this.subs.sink = this.resultService.create(request).subscribe((result) => {
      this.redirectToResultsPage(result.id);
    }, error => {
      this.errorHandlerService.handleError(error);
    });
  }

  private redirectToResultsPage(resultId: number): void {
    console.log(this.userAnswers);
    this.router.navigate(['/active-test', 'result', resultId]);
  }

}
