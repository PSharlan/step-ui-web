import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {ResultService} from '../../shared/service/test/result.service';
import {Result} from '../../shared/model/test/result-models';
import {throwError} from 'rxjs';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';

@Component({
  selector: 'app-result-test',
  templateUrl: './result-test.component.html',
  styleUrls: ['./result-test.component.css']
})
export class ResultTestComponent implements OnInit {
  public result!: Result;
  public correctAmount: number;
  public wrongAmount: number;
  public time: string;

  constructor(private route: ActivatedRoute,
              private resultService: ResultService,
              private errorHandlerService: ErrorHandlerService) {
    this.correctAmount = 0;
    this.wrongAmount = 0;
    this.time = '';
  }

  ngOnInit(): void {
    this.route.params.pipe(
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
      switchMap((params: Params) => {
        return this.resultService.getById(params.id);
      }),
      tap((result: Result) => {
        console.log(result);
        this.result = result;
        this.countCorrectAndWrongAmount();
        this.countTime();
      })
    ).subscribe();
  }

  private countCorrectAndWrongAmount(): void {
    for (const answer of this.result.answers) {
      if (answer.isCorrect) {
        this.correctAmount++;
      }
    }
    this.wrongAmount = this.result.answers.length - this.correctAmount;
  }

  private countTime(): void {
    const date = new Date(this.result.timeSpent);
    const hours = '0' + date.getHours();
    const minutes = '0' + date.getMinutes();
    const seconds = '0' + date.getSeconds();
    this.time = hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

}
