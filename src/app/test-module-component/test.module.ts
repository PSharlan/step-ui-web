import {NgModule} from '@angular/core';
import {ModuleMenuComponent} from './module-menu/module-menu.component';
import {RouterModule} from '@angular/router';
import {MyQuestionsComponent} from './my-questions/my-questions.component';
import {MyTestsComponent} from './my-tests/my-tests.component';
import {CommonModule} from '@angular/common';
import {CreateTestComponent} from './create-test/create-test.component';
import {CreateQuestionDialogComponent} from './create-question-dialog/create-question-dialog.component';
import {AppMaterialModule} from '../app-material.module';

@NgModule({
  declarations: [
    ModuleMenuComponent,
    MyQuestionsComponent,
    MyTestsComponent,
    CreateTestComponent,
    CreateQuestionDialogComponent
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '', component: ModuleMenuComponent, children: [
          {path: '', redirectTo: '/test-module/tests', pathMatch: 'full'},
          {path: 'tests', component: MyTestsComponent},
          {path: 'questions', component: MyQuestionsComponent}
        ]
      }
    ]),
    CommonModule,
    AppMaterialModule
  ],
  exports: [RouterModule]
})

export class TestModule {
}
