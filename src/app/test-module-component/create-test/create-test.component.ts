import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {originalOrder} from 'src/app/shared/helpers/comparators/originalOrder.helper';
import {getProperEnding} from 'src/app/shared/helpers/validators/proper-word-ending.helper';
import {QuestionPreview} from 'src/app/shared/model/test/question-models';
import {TestCreationRequest} from 'src/app/shared/model/test/test-models';
import {AuthService} from 'src/app/shared/service/auth.service';
import {ErrorHandlerService} from 'src/app/shared/service/error-handler.service';
import {DictionaryService} from 'src/app/shared/service/test/dictionary.service';
import {QuestionService} from 'src/app/shared/service/test/question.service';
import {TestService} from 'src/app/shared/service/test/test.service';
import {SubSink} from 'subsink';
import {CreateQuestionDialogComponent} from '../create-question-dialog/create-question-dialog.component';

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.css']
})
export class CreateTestComponent implements OnInit, OnDestroy {

  originalOrder = originalOrder;

  testLanguages!: any;
  testDirections!: any;
  questionTypes!: any;
  questionLanguages!: any;
  questionLevels!: any;

  allQuestions: QuestionPreview[] = [];
  properAllQuestionsWord = '';

  usedQuestions: QuestionPreview[] = [];
  properUsedQuestionsWord = '';

  @Input()
  testName!: string;
  @Input()
  testLanguage!: string;
  @Input()
  testDirection!: string;

  private subs = new SubSink();
  private allQuestionsPage = 0;
  private allQuesionsSize = 100;

  constructor(private dialog: MatDialog,
              private dictionaryService: DictionaryService,
              private questionService: QuestionService,
              private testService: TestService,
              private router: Router,
              private authService: AuthService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.loadEnums();
    this.loadQuestions();
    this.updateQuestionsEndings();
  }

  openCreateQuestionDialog(): void {
    const dialogRef = this.dialog.open(CreateQuestionDialogComponent, {
      height: '100%'
    });
    this.subs.sink = dialogRef.afterClosed().subscribe(result => {
      this.loadQuestions();
      this.updateQuestionsEndings();
    });
  }

  checkInputs(): boolean {
    return !!this.testName && !!this.testDirection && !!this.testLanguage && this.usedQuestions && this.usedQuestions.length !== 0;
  }

  saveTest(): void {
    if (!this.checkInputs()) {
      return;
    }

    const level = this.intToLevel(
      Math.round(
        this.usedQuestions.reduce(
          (currSum, question) => currSum + this.levelToInt(question.level), 0)
        / this.usedQuestions.length
      )
    );

    const request: TestCreationRequest = {
      authorId: this.authService.getUserId(),
      direction: this.testDirection,
      language: this.testLanguage,
      level,
      name: this.testName,
      questionIds: this.usedQuestions.map((question) => question.id),
    };

    this.testService.create(request).subscribe((test) => {
      this.router.navigate(['/test-module']);
    }, (error) => {
      this.errorHandlerService.handleError(error);
    });
  }

  private loadEnums(): void {
    this.testLanguages = this.dictionaryService.getTestLanguages();
    this.testDirections = this.dictionaryService.getTestDirections();
    this.questionLanguages = this.dictionaryService.getQuestionLanguages();
    this.questionLevels = this.dictionaryService.getQuestionLevels();
    this.questionTypes = this.dictionaryService.getQuestionTypes();
  }

  private loadQuestions(): void {
    this.subs.sink = this.questionService.getAll(this.allQuestionsPage, this.allQuesionsSize).subscribe((questions) => {
      this.allQuestions = questions.content;
      this.properAllQuestionsWord = getProperEnding('вопрос', this.allQuestions.length);
    }, (error) => {
      this.errorHandlerService.handleError(error);
    });
  }

  private updateQuestionsEndings(): void {
    this.properAllQuestionsWord = getProperEnding('вопрос', this.allQuestions.length);
    this.properUsedQuestionsWord = getProperEnding('вопрос', this.usedQuestions.length);
  }

  private levelToInt(toConvert: string): number {
    let i = 0;
    for (const key of Object.keys(this.questionLevels)) {
      if (key === toConvert) {
        return i;
      }
      i++;
    }

    return -1;
  }

  private intToLevel(toConvert: number): string {
    const keys = Object.keys(this.questionLevels);

    return (0 <= toConvert && toConvert < keys.length) ? keys[toConvert] : '';
  }

  drop(event: CdkDragDrop<QuestionPreview[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }

    this.updateQuestionsEndings();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
