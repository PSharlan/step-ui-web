import {Component, Input, OnInit} from '@angular/core';
import {Question, QuestionCreationRequest} from 'src/app/shared/model/test/question-models';
import {AuthService} from 'src/app/shared/service/auth.service';
import {QuestionService} from 'src/app/shared/service/test/question.service';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {DictionaryService} from 'src/app/shared/service/test/dictionary.service';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {originalOrder} from 'src/app/shared/helpers/comparators/originalOrder.helper';
import {FilesHelper} from 'src/app/shared/helpers/file.helper';

@Component({
  selector: 'app-create-question-dialog',
  templateUrl: './create-question-dialog.component.html',
  styleUrls: ['./create-question-dialog.component.css']
})
export class CreateQuestionDialogComponent implements OnInit {

  originalOrder = originalOrder;

  @Input()
  questionCreationRequest: QuestionCreationRequest = {
    authorId: -1,
    codeBlock: '',
    type: '',
    text: '',
    language: '',
    level: '',
    options: []
  };

  files!: any;

  questionTypes!: any;
  questionLanguages!: any;
  questionLevels!: any;

  private currentUserId = 0;
  private filesHelper = new FilesHelper();

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private authService: AuthService,
              private questionService: QuestionService,
              private dictionaryService: DictionaryService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.loadImages();
    this.loadEnums();
    this.getCurrentUserId();
  }

  public addOption(): void {
    this.questionCreationRequest.options.push({
      text: '',
      isCorrect: false
    });
  }

  public saveQuestion(): void {
    this.questionCreationRequest.authorId = this.currentUserId;
    this.questionService.create(this.questionCreationRequest).subscribe(question => {
      document.getElementById('closeModal')?.click();
    }, error => {
      this.errorHandlerService.handleError(error);
    });
  }

  public checkInputs(): boolean {
    return !!this.questionCreationRequest.text && !!this.questionCreationRequest.type &&
      !!this.questionCreationRequest.language && !!this.questionCreationRequest.level && this.checkOptions();
  }

  public handleFileInputEvent(event: Event): void {
    const fileList  = (event.target as HTMLInputElement)?.files;
    if (fileList != null) {
      this.handleFileInput(fileList);
    }
  }

  public handleFileInput(files: FileList): void {
    this.files = this.filesHelper.handleFileInput(files);
  }

  public deleteFile(index: number): void {
    this.files = this.filesHelper.deleteFile(index);
  }

  private checkOptions(): boolean {
    let foundCorrectAnswer = false;
    for (const option of this.questionCreationRequest.options) {
      if (!option || (foundCorrectAnswer && option.isCorrect && this.questionCreationRequest.type === 'SELECT_ONE')) {
        return false;
      }

      if (option.isCorrect) {
        foundCorrectAnswer = true;
      }
    }

    return foundCorrectAnswer;
  }

  private loadImages(): void {
    this.matIconRegistry.addSvgIcon(
      'mark-as-correct',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/check-mark.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'mark-as-wrong',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/cross.svg')
    );
  }

  private loadEnums(): void {
    this.questionLanguages = this.dictionaryService.getQuestionLanguages();
    this.questionLevels = this.dictionaryService.getQuestionLevels();
    this.questionTypes = this.dictionaryService.getQuestionTypes();
  }

  private getCurrentUserId(): void {
    this.currentUserId = +this.authService.getUserId();
  }
}
