import {Component, OnDestroy, OnInit} from '@angular/core';
import {getProperEnding} from 'src/app/shared/helpers/validators/proper-word-ending.helper';
import {ErrorHandlerService} from 'src/app/shared/service/error-handler.service';
import {DictionaryService} from 'src/app/shared/service/test/dictionary.service';
import {SubSink} from 'subsink';
import {TestPreview} from '../../shared/model/test/test-models';
import {TestService} from '../../shared/service/test/test.service';

@Component({
  selector: 'app-my-tests',
  templateUrl: './my-tests.component.html',
  styleUrls: ['./my-tests.component.css']
})
export class MyTestsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  tests: TestPreview[] = [];
  currentPage = 0;
  pageLimit = 10;
  properTestsLength = '';

  testLanguages: any;

  constructor(private testService: TestService,
              private errorHandler: ErrorHandlerService,
              private dictionaryService: DictionaryService) {
  }

  ngOnInit(): void {
    this.loadEnums();
    this.loadTests();
  }

  private loadTests(): void {
    this.subs.sink = this.testService.getAll(this.currentPage, this.pageLimit).subscribe(tests => {
      this.tests = tests.content;
      this.properTestsLength = getProperEnding('тест', this.tests.length);
    }, error => {
      this.errorHandler.handleError(error);
    });
  }

  private loadEnums(): void {
    this.testLanguages = this.dictionaryService.getTestLanguages();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
