import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubSink} from 'subsink';
import {QuestionPreview} from '../../shared/model/test/question-models';
import {QuestionService} from '../../shared/service/test/question.service';
import {CreateQuestionDialogComponent} from '../create-question-dialog/create-question-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {getProperEnding} from 'src/app/shared/helpers/validators/proper-word-ending.helper';
import {tap} from 'rxjs/operators';
import {DictionaryService} from 'src/app/shared/service/test/dictionary.service';
import {interval} from 'rxjs';

@Component({
  selector: 'app-my-questions',
  templateUrl: './my-questions.component.html',
  styleUrls: ['./my-questions.component.css']
})
export class MyQuestionsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  questions: QuestionPreview[] = [];
  currentPage = 0;
  pageLimit = 100;
  properQuestionsLength = '';

  questionTypes!: any;
  questionLanguages!: any;
  questionLevels!: any;

  constructor(private questionService: QuestionService,
              private dialog: MatDialog,
              private errorHandler: ErrorHandlerService,
              private dictionaryService: DictionaryService) {
  }

  ngOnInit(): void {
    this.loadEnums();
    this.loadQuestions();
  }

  private loadQuestions(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
    this.subs.sink = this.questionService.getAll(this.currentPage, this.pageLimit).subscribe(questions => {
      this.questions = questions.content;
      this.properQuestionsLength = getProperEnding('вопрос', this.questions.length);
    }, error => {
      this.errorHandler.handleError(error);
    });
  }

  openCreateQuestionDialog(): void {
    const dialogRef = this.dialog.open(CreateQuestionDialogComponent, {
      height: '100%'
    });
    this.subs.sink = dialogRef.afterClosed().subscribe(result => {
      this.loadQuestions();
    });
  }

  private loadEnums(): void {
    this.questionLanguages = this.dictionaryService.getQuestionLanguages();
    this.questionLevels = this.dictionaryService.getQuestionLevels();
    this.questionTypes = this.dictionaryService.getQuestionTypes();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
