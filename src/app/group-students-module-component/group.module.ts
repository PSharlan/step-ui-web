import {NgModule} from '@angular/core';
import {GroupModuleMenuComponent} from './group-module-menu/group-module-menu.component';
import {RouterModule} from '@angular/router';
import {MyGroupComponent} from './my-group/my-group.component';
import {AppMaterialModule} from '../app-material.module';
import {CommonModule} from '@angular/common';
import {StudentConfirmationDialogComponent} from './dialog/request-confirmation-dialog-wrapper/student-confirmation-dialog.component';
import {ConfirmStudentComponent} from './dialog/student-registration-request-dialog/confirm-student.component';
import {AddStudentComponent} from './dialog/add-student/add-student.component';
import {MyHomeworksComponent} from './my-homeworks/my-homeworks.component';
import {TeacherHomeworksComponent} from './teacher-homeworks/teacher-homeworks.component';
import {AddHomeworkDialogComponent} from './dialog/add-homework-dialog/add-homework-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MarkColorDirective} from '../shared/directives/mark-color.directive';
import {CheckHomeworkDialogComponent} from './dialog/check-homework-dialog/check-homework-dialog.component';

@NgModule({
  declarations: [
    GroupModuleMenuComponent,
    MyGroupComponent,
    StudentConfirmationDialogComponent,
    ConfirmStudentComponent,
    AddStudentComponent,
    MyHomeworksComponent,
    TeacherHomeworksComponent,
    AddHomeworkDialogComponent,
    MarkColorDirective,
    CheckHomeworkDialogComponent
  ],
  imports: [
    AppMaterialModule,
    RouterModule.forChild([
      {
        path: 'my-group/:id', component: GroupModuleMenuComponent, children: [
          {path: '', component: MyGroupComponent},
          {path: 'homeworks/student', component: MyHomeworksComponent},
          {path: 'homeworks/teacher', component: TeacherHomeworksComponent}
        ]
      }
    ]),
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
})

export class GroupModule {
}
