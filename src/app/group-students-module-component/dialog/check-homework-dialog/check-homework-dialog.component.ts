import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SubSink} from 'subsink';
import {HomeworkAnswerService} from '../../../shared/service/homework/answer.service';
import {Answer, AnswerUpdateRequest} from '../../../shared/model/homework/answer-models';
import {Homework} from '../../../shared/model/homework/homework-models';
import {NotificationService} from '../../../shared/service/notification.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {StudentPreview} from '../../../shared/model/account/student-models';

@Component({
  selector: 'app-check-homework-dialog',
  templateUrl: './check-homework-dialog.component.html',
  styleUrls: ['./check-homework-dialog.component.css']
})
export class CheckHomeworkDialogComponent implements OnInit, OnDestroy {

  marks = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
  mark!: number;
  retake = false;
  answers: Answer[] = [];
  form!: FormGroup;
  lastAnswer!: Answer;
  submitted = false;
  private subs = new SubSink();

  constructor(public dialogRef: MatDialogRef<CheckHomeworkDialogComponent>,
              private homeworkAnswerService: HomeworkAnswerService,
              private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: { homework: Homework, student: StudentPreview }) {
  }

  ngOnInit(): void {
    this.getHomework();
  }

  private getHomework(): void {
    this.subs.sink = this.homeworkAnswerService.getByHomeworkAndStudentId(this.data.homework.id, this.data.student.id)
      .subscribe((answers: Answer[]) => {
        this.answers = answers;

        this.lastAnswer = answers[answers.length - 1];
        this.retake = this.lastAnswer.answerStatus === 'RETAKE';
        this.initializeForm();

      }, error => {
        this.notificationService.showErrorNotification(error);
      });
  }

  private initializeForm(): void {
    console.log(this.lastAnswer);
    this.form = this.formBuilder.group({
      mark: [this.lastAnswer.realGrade],
      teacherComment: [null]
    });
  }

  public submit(): void {
    this.submitted = true;
    const request: AnswerUpdateRequest = {
      answerResourceUrl: this.lastAnswer.answerResourceUrl,
      answerStatus: this.getStatus(),
      id: this.lastAnswer.id,
      realGrade: this.form.value.mark,
      teacherComment: this.form.value.teacherComment
    };
    console.log(request);
    this.subs.sink = this.homeworkAnswerService.update(request).subscribe((answer: Answer) => {
      this.submitted = false;
      this.notificationService.showDefaultNotification('Изменения сохранены');
      this.closeDialog(answer);
    }, error => {
      this.submitted = false;
      this.notificationService.showErrorNotification(error);
    });
  }

  private getStatus(): string {
    return this.retake ? 'RETAKE' : 'ASSESSED';
  }

  public closeDialog(data?: Answer): void {
    this.dialogRef.close(data);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
