import {Component, Inject, OnInit} from '@angular/core';
import {FilesHelper} from '../../../shared/helpers/file.helper';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HomeworkCreationRequest} from '../../../shared/model/homework/homework-models';
import {AuthService} from '../../../shared/service/auth.service';
import {HomeworkService} from '../../../shared/service/homework/homework.service';
import {SubSink} from 'subsink';
import {NotificationService} from '../../../shared/service/notification.service';

@Component({
  selector: 'app-add-homework-dialog',
  templateUrl: './add-homework-dialog.component.html',
  styleUrls: ['./add-homework-dialog.component.css']
})
export class AddHomeworkDialogComponent implements OnInit {

  file!: any;
  form!: FormGroup;
  submitted = false;
  private subs = new SubSink();

  private filesHelper = new FilesHelper();

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<AddHomeworkDialogComponent>,
              private authService: AuthService,
              private homeworkService: HomeworkService,
              private notificationService: NotificationService,
              @Inject(MAT_DIALOG_DATA) public data: { groupId: number }) {
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  private initializeForm(): void {
    const todayDate = new Date();
    const tillDate = new Date();
    tillDate.setDate(tillDate.getDate() + 14);
    this.form = this.formBuilder.group({
      lessonDate: [todayDate, [Validators.required]],
      deadLineDate: [tillDate, [Validators.required]],
      topic: [null, [Validators.required]],
      description: [null, [Validators.required]],
      file: [null]
    });
  }

  public handleFileInputEvent(event: Event): void {
    const fileList = (event.target as HTMLInputElement)?.files;
    if (fileList != null) {
      this.handleFileInput(fileList);
    }
  }

  public handleFileInput(files: FileList): void {
    this.file = this.filesHelper.handleFileInput(files);
  }

  public submit(): void {
    this.submitted = true;
    const request: HomeworkCreationRequest = {
      deadLine: this.form.value.deadLineDate,
      lessonDate: this.form.value.lessonDate,
      description: this.form.value.description,
      groupId: this.data.groupId,
      teacherId: this.authService.getUserId(),
      topic: this.form.value.topic,
      taskResourceUrl: this.file.fileList[0].name
    };

    this.subs.sink = this.homeworkService.create(request).subscribe( () => {
      this.notificationService.showDefaultNotification('Задание успешно создано');
      this.closeDialog(true);
      this.submitted = false;
    }, error => {
      this.notificationService.showErrorNotification(error);
      this.submitted = false;
    });
  }

  public closeDialog(result: boolean): void {
    this.dialogRef.close(result);
  }
}
