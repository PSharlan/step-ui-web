import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-student-confirmation-dialog',
  templateUrl: './student-confirmation-dialog.component.html',
  styleUrls: ['./student-confirmation-dialog.component.css']
})
export class StudentConfirmationDialogComponent implements OnInit {
  groupId!: number;

  constructor(public dialogRef: MatDialogRef<StudentConfirmationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: { groupId: number }) {
    this.groupId = data.groupId;
  }

  ngOnInit(): void {
  }

  public closeDialog(): void {
    this.dialogRef.close();
  }

}
