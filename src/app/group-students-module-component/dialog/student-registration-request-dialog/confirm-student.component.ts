import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {catchError, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {RegistrationRequestService} from '../../../shared/service/account/registration-request.service';
import {RegistrationRequest} from '../../../shared/model/account/registration-request-models';
import {NotificationService} from '../../../shared/service/notification.service';
import {SubSink} from 'subsink';
import {throwError} from 'rxjs';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';

@Component({
  selector: 'app-confirm-student',
  templateUrl: './confirm-student.component.html',
  styleUrls: ['./confirm-student.component.css']
})
export class ConfirmStudentComponent implements OnInit, OnDestroy {
  @Input() groupId!: number;
  public requests: RegistrationRequest[] = [];
  private subs = new SubSink();

  constructor(private route: ActivatedRoute,
              private registrationRequestService: RegistrationRequestService,
              private notificationService: NotificationService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.getStudents();
  }

  private getStudents(): void {
    this.subs.sink = this.registrationRequestService.getAllByGroupId(this.groupId).pipe(
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
      tap((students: RegistrationRequest[]) => {
        this.requests = students;
      })
    ).subscribe();
  }

  public confirmRequest(id: number, name: string, lastName: string): void {
    this.subs.sink = this.registrationRequestService.confirmById(id).subscribe(() => {
      this.notificationService.showDefaultNotification(`${name} ${lastName} был подтвержден`);
      this.requests = this.requests.filter(student => student.id !== id);
    });
  }

  public rejectRequest(id: number, name: string, lastName: string): void {
  } // TODO make a request to a server with a deregistering status

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
