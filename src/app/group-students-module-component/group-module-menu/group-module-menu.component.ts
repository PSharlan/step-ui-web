import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from '../../shared/service/shared.service';
import {ActivatedRoute, Params} from '@angular/router';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Group} from '../../shared/model/account/group-models';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {Student} from '../../shared/model/account/student-models';
import {SubSink} from 'subsink';
import {GroupService} from '../../shared/service/account/group.service';
import {AuthService} from '../../shared/service/auth.service';

@Component({
  selector: 'app-group-module-menu',
  templateUrl: './group-module-menu.component.html',
  styleUrls: ['./group-module-menu.component.css']
})
export class GroupModuleMenuComponent implements OnInit, OnDestroy {
  public groupName!: string;
  public groupId!: string;
  public group!: Group;
  public studentProfile!: Student;
  public userRole!: string;
  private subs = new SubSink();

  constructor(private sharedService: SharedService,
              private cdRef: ChangeDetectorRef,
              private route: ActivatedRoute,
              private errorHandlerService: ErrorHandlerService,
              private groupService: GroupService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.getUserRole();
    this.getGroupData();
  }

  private getGroupData(): void {
    this.subs.sink = this.route.params.pipe(
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
      switchMap((params: Params) => {
        return this.groupService.getById(params.id);
      }),
      tap((group: Group) => {
        this.groupId = group.id.toString();
        this.groupName = `${group.subjectOfStudy}/${group.name}`;
        this.sharedService.emitChange(group);
      })
    ).subscribe();
  }

  public getUserRole(): string {
    return this.authService.getUserRole().toLowerCase();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
