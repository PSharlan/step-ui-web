import {Component, OnDestroy, OnInit} from '@angular/core';
import {GroupService} from '../../shared/service/account/group.service';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../shared/service/shared.service';
import {MatDialog} from '@angular/material/dialog';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {StudentPreview} from '../../shared/model/account/student-models';
import {SubSink} from 'subsink';
import {Answer} from '../../shared/model/homework/answer-models';
import {HomeworkService} from '../../shared/service/homework/homework.service';
import {Homework} from '../../shared/model/homework/homework-models';
import {AddHomeworkDialogComponent} from '../dialog/add-homework-dialog/add-homework-dialog.component';
import {switchMap, tap} from 'rxjs/operators';
import {CheckHomeworkDialogComponent} from '../dialog/check-homework-dialog/check-homework-dialog.component';

@Component({
  selector: 'app-teacher-homeworks',
  templateUrl: './teacher-homeworks.component.html',
  styleUrls: ['./teacher-homeworks.component.css']
})

export class TeacherHomeworksComponent implements OnInit, OnDestroy {
  public students: StudentPreview[] = [];
  public rows: TableRow[] = [];
  public homeworks: Homework[] = [];
  public student!: StudentPreview;
  DEFAULT_EMPTY_GRADE = -1;
  private subs = new SubSink();
  private groupId!: number;

  constructor(private groupService: GroupService,
              private route: ActivatedRoute,
              private sharedService: SharedService,
              private dialog: MatDialog,
              private errorHandlerService: ErrorHandlerService,
              private homeworkService: HomeworkService) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.subs.sink = this.sharedService.changeEmitted$.pipe(
      tap((group) => {
        this.students = group.students;
        this.groupId = group.id;
      }),
      switchMap(() => {
        return this.homeworkService.getFullByGroupId(this.groupId);
      }),
      tap((page) => {
        // so that new homework comes first
        this.homeworks = page.content.sort((a, b) => new Date(b.lessonDate).getTime() - new Date(a.lessonDate).getTime());
        this.initArray();
        this.processData();
      })
    ).subscribe();
  }

  private initArray(): void {
    this.rows = [];
    for (const student of this.students) {
      this.rows.push(new TableRow(student, this.homeworks.length));
    }
  }

  private processData(): void {
    for (let homeworkIndex = 0; homeworkIndex < this.homeworks.length; homeworkIndex++) {
      this.homeworks[homeworkIndex].answers.sort((a, b) => a.id - b.id);
      for (const answer of this.homeworks[homeworkIndex].answers) {
        for (const row of this.rows) {
          if (row.student.id === answer.studentId) {
            row.answers[homeworkIndex] = answer;
            break;
          }
        }
      }
    }
  }

  public openAddHomeworkDialog(): void {
    const dialogRef = this.dialog.open(AddHomeworkDialogComponent, {
      data: {
        groupId: this.groupId
      },
      panelClass: 'dialog-config',
      width: '800px'
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        return;
      }
      this.getData();
    });
  }

  public openCheckHomeworkDialog(answer: Answer): void {
    this.student = this.rows[this.findCurrentStudentIndex(answer.studentId)].student;
    const currentHomework = this.homeworks.find(homework => homework.id === answer.homeworkId);
    const dialogRef = this.dialog.open(CheckHomeworkDialogComponent, {
      data: {
        homework: currentHomework,
        student: this.student
      },
      panelClass: 'dialog-config'
    });
    this.subs.sink = dialogRef.afterClosed().subscribe((newAnswer: Answer) => {
      this.updateAnswer(newAnswer);
    });
  }

  private updateAnswer(newAnswer: Answer): void {
    if (!newAnswer) {
      return;
    }
    for (const answer of this.rows[this.findCurrentStudentIndex(newAnswer.studentId)].answers) {
      if (answer.id === newAnswer.id && answer.homeworkId === newAnswer.homeworkId) {
        answer.realGrade = newAnswer.realGrade;
        answer.answerStatus = newAnswer.answerStatus;
        break;
      }
    }
  }

  private findCurrentStudentIndex(studentId: number): number {
    let studentIndex = 0;
    for (const row of this.rows) {
      if (row.student.id === studentId) {
        break;
      }
      studentIndex++;
    }
    return studentIndex;
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}

class TableRow {
  public student: StudentPreview;
  public answers: Answer[];

  constructor(student: StudentPreview, homeworkSize: number) {
    this.student = student;
    this.answers = [];
    for (let i = 0; i < homeworkSize; i++) {
      this.answers.push({
        id: -1,
        created: '',
        realGrade: -1,
        studentId: -1,
        teacherComment: '',
        studentComment: '',
        answerResourceUrl: '',
        answerStatus: '',
        homeworkId: -1
      });
    }
  }
}
