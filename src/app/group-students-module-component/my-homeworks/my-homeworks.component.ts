import {Component, OnDestroy, OnInit} from '@angular/core';
import {FilesHelper} from '../../shared/helpers/file.helper';
import {HomeworkService} from '../../shared/service/homework/homework.service';
import {SubSink} from 'subsink';
import {ActivatedRoute} from '@angular/router';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {HomeworkWithAnswerPreview} from '../../shared/model/homework/homework-models';
import {Page} from '../../shared/model/page-model';
import {SharedService} from '../../shared/service/shared.service';
import {HomeworkAnswerService} from '../../shared/service/homework/answer.service';
import {AuthService} from '../../shared/service/auth.service';
import {Answer, AnswerCreationRequest} from '../../shared/model/homework/answer-models';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NotificationService} from '../../shared/service/notification.service';

@Component({
  selector: 'app-my-homeworks',
  templateUrl: './my-homeworks.component.html',
  styleUrls: ['./my-homeworks.component.css']
})
export class MyHomeworksComponent implements OnInit, OnDestroy {

  private filesHelper = new FilesHelper();
  private subs = new SubSink();
  public homeworks: HomeworkWithAnswerPreview[] = [];
  public homeworkAnswersMap = new Map<number, Answer[]>();
  public answers: Answer[] = [];
  public sendAnswerForm!: FormGroup;
  public submitted = false;
  public fileToUpload!: any;

  constructor(private homeworkService: HomeworkService,
              private route: ActivatedRoute,
              private sharedService: SharedService,
              private homeworkAnswerService: HomeworkAnswerService,
              private authService: AuthService,
              private formBuilder: FormBuilder,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.getHomeworks();
  }

  private getHomeworks(): void {
    this.subs.sink = this.sharedService.changeEmitted$.pipe(
      switchMap((group) => {
        return this.homeworkService.getByGroupIdAndStudentId(group.id, this.authService.getUserId());
      }),
      tap((page: Page<HomeworkWithAnswerPreview[]>) => {
        this.homeworks = page.content.sort((a, b) => {
          return new Date(b.lessonDate).getTime() - new Date(a.lessonDate).getTime();
        });
      })
    ).subscribe();
  }

  public getCurrentHomework(homeworkId: number): void {
    if (this.sendAnswerForm) {
      this.sendAnswerForm.reset();
      this.fileToUpload = null;
    }
    this.sendAnswerForm = this.formBuilder.group({
      studentComment: [null],
      file: [null]
    });
    this.subs.sink = this.homeworkAnswerService.getByHomeworkAndStudentId(homeworkId, this.authService.getUserId())
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.homeworkAnswersMap.set(homeworkId, []);
          }
          return throwError(error);
        })
      ).subscribe((answers: Answer[]) => {
        this.homeworkAnswersMap.set(homeworkId, answers);
      });
  }

  public submit(id: number): void {
    this.submitted = true;

    const request: AnswerCreationRequest = {
      answerResourceUrl: 'answer.zip',
      // this.fileToUpload.fileList[0].name,
      homeworkId: id,
      studentId: this.authService.getUserId(),
      studentComment: this.sendAnswerForm.value.studentComment
    };

    this.subs.sink = this.homeworkAnswerService.create(request).subscribe((answer: Answer) => {
      this.notificationService.showDefaultNotification('Ответ отправлен');
      this.updateAnswerStatus(id);
      this.homeworkAnswersMap.get(id)?.push(answer);
      this.sendAnswerForm.reset();
      this.fileToUpload = null;
      this.submitted = false;
    }, error => {
      this.notificationService.showErrorNotification(error.error);
      this.submitted = false;
    });
  }

  private updateAnswerStatus(id: number): void {
    for (let i = 0; i < this.homeworks.length; i++) {
      if (this.homeworks[i].id === id) {
        this.homeworks[i].answerStatus = 'PENDING';
        break;
      }
    }
  }

  public handleFileInputEvent(event: Event): void {
    const fileList = (event.target as HTMLInputElement)?.files;
    if (fileList != null) {
      this.handleFileInput(fileList);
    }
  }

  public handleFileInput(files: FileList): void {
    this.fileToUpload = this.filesHelper.handleFileInput(files);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
