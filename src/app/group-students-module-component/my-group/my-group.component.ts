import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {GroupService} from '../../shared/service/account/group.service';
import {SharedService} from '../../shared/service/shared.service';
import {Student, StudentPreview} from '../../shared/model/account/student-models';
import {MatDialog} from '@angular/material/dialog';
import {StudentConfirmationDialogComponent} from '../dialog/request-confirmation-dialog-wrapper/student-confirmation-dialog.component';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {AuthService} from '../../shared/service/auth.service';
import {SubSink} from 'subsink';
import {StudentService} from '../../shared/service/account/student.service';
import {RegistrationRequestService} from '../../shared/service/account/registration-request.service';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {forkJoin, Observable, throwError} from 'rxjs';
import {RegistrationRequest} from '../../shared/model/account/registration-request-models';
import {PlayerService} from '../../shared/service/achievement/player.service';
import {Player} from '../../shared/model/achievement/player-models';
import {AchievementPreview} from '../../shared/model/achievement/achievement-models';
import {MatTable} from '@angular/material/table';
import {CdkTable} from '@angular/cdk/table';

@Component({
  selector: 'app-my-group',
  templateUrl: './my-group.component.html',
  styleUrls: ['./my-group.component.css']
})
export class MyGroupComponent implements OnInit, OnDestroy {
  public groupId!: number;
  public students: StudentPreview[] = [];
  public studentProfile!: Student;
  public numberOfApplications!: number;
  public dataSource: StudentView[] = [];
  public displayedColumns: string[] = ['name', 'status', 'achievements', 'score'];
  private subs = new SubSink();
  @ViewChild(MatTable) table!: MatTable<CdkTable<any>>;

  constructor(private groupService: GroupService,
              private route: ActivatedRoute,
              private sharedService: SharedService,
              private dialog: MatDialog,
              private errorHandlerService: ErrorHandlerService,
              private authService: AuthService,
              private studentService: StudentService,
              private registrationRequestService: RegistrationRequestService,
              private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.getStudentProfile();
    this.checkChanges();
  }

  private checkChanges(): void {
    this.subs.sink = this.sharedService.changeEmitted$
      .pipe(
        switchMap((group) => {
          const playerObservables: Observable<Player>[] = [];
          this.students = group.students;
          this.groupId = group.id;
          for (const student of group.students) {
            playerObservables.push(this.playerService.getByUserId(student.id));
          }
          return forkJoin(playerObservables);
        })
      )
      .subscribe((players: Player[]) => {
          this.dataSource = [];
          for (const student of this.students) {
            let achievements: AchievementPreview[] = [];
            for (const player of players) {
              if (student.id === player?.userId) {
                achievements = player.achievements;
                break;
              }
            }
            this.dataSource.push(new StudentView(student, achievements));
          }
          console.log(this.dataSource);
          this.table.renderRows();
        }, error => {
          this.errorHandlerService.handleError(error);
        }
      );
  }

  private getStudentProfile(): void {
    if (this.getUserRole() !== 'STUDENT') {
      this.getNumberOfApplications();
      return;
    }
    this.subs.sink = this.studentService.getById(this.authService.getUserId()).subscribe(
      (userProfile: Student) => {
        this.studentProfile = userProfile;
      }, error => {
        this.errorHandlerService.handleError(error);
      });
  }

  private getNumberOfApplications(): void {
    this.subs.sink = this.route.params.pipe(
      switchMap((params: Params) => {
        return this.registrationRequestService.getAllByGroupId(params.id);
      }),
      tap((students: RegistrationRequest[]) => {
        this.numberOfApplications = students.length;
      }),
      catchError((error) => {
        this.errorHandlerService.handleError(error);
        return throwError(error);
      }),
    ).subscribe();
  }

  public getUserRole(): string {
    return this.authService.getUserRole();
  }

  openStudentConfirmationDialog(): void {
    const dialogRef = this.dialog.open(StudentConfirmationDialogComponent, {
      data: {
        groupId: this.groupId
      },
      width: '800px',
      height: '720px'
    });
    this.subs.sink = dialogRef.afterClosed().pipe(
      switchMap(() => {
        return this.registrationRequestService.getAllByGroupId(this.groupId);
      }),
      tap((students) => {
        this.numberOfApplications = students.length;
      })
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}

class StudentView {

  public id: number;
  public name: string;
  public lastName: string;
  public rating: number;
  public email: string;
  public achievements: AchievementPreview[];

  constructor(student: StudentPreview, achievements: AchievementPreview[]) {

    this.id = student.id;
    this.name = student.name;
    this.lastName = student.lastName;
    this.rating = student.rating;
    this.email = student.email;
    this.achievements = achievements.slice(-3);
  }
}
