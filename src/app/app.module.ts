import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainLayoutComponent} from './shared/components/main-layout/main-layout.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RegistrationLayoutComponent} from './shared/components/registration-layout/registration-layout.component';
import {ActiveTestLayoutComponent} from './shared/components/active-test-layout/active-test-layout.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppMaterialModule} from './app-material.module';
import {NotificationDialogComponent} from './shared/components/notification-dialog/notification-dialog.component';
import {LoadingInterceptor} from './shared/interceptors/loading.interceptor';
import { AchievementNotificationDialogComponent } from './shared/components/achievement-notification-dialog/achievement-notification-dialog.component';

const INTERCEPTOR_LOADING_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: LoadingInterceptor
};

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    RegistrationLayoutComponent,
    ActiveTestLayoutComponent,
    NotificationDialogComponent,
    AchievementNotificationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  providers: [INTERCEPTOR_LOADING_PROVIDER],
  bootstrap: [AppComponent]
})
export class AppModule {
}
