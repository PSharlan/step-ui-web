import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/service/auth.service';

@Component({
  selector: 'app-achievement-module-menu',
  templateUrl: './achievement-module-menu.component.html',
  styleUrls: ['./achievement-module-menu.component.css']
})
export class AchievementModuleMenuComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getUserRole();
  }

  getUserRole(): string {
    return this.authService.getUserRole();
  }

}
