import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CreateAchievementPacksDialogComponent} from '../dialog/create-achievement-packs-dialog/create-achievement-packs-dialog.component';
import {SubSink} from 'subsink';
import {GroupAchievement} from '../../shared/model/achievement/group-achievement-models';
import {AchievementGroupService} from '../../shared/service/achievement/achievement-group.service';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {AchievementPreview, actions} from '../../shared/model/achievement/achievement-models';

@Component({
  selector: 'app-my-achievement-packs',
  templateUrl: './my-achievement-packs.component.html',
  styleUrls: ['./my-achievement-packs.component.css']
})
export class MyAchievementPacksComponent implements OnInit, OnDestroy {

  groupAchievements: GroupAchievement[] = [];
  filteredAchievements: AchievementPreview[] = [];
  actionsList: string[] = actions;
  selectedAction = 'TEST_CREATING';
  private subs = new SubSink();

  constructor(private dialog: MatDialog,
              private groupAchievementService: AchievementGroupService,
              private errorHandleService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.getAchievementPacks();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  openCreateAchievementPacksDialog(): void {
    this.dialog.open(CreateAchievementPacksDialogComponent, {
      panelClass: 'dialog-config',
      height: '700px',
    });
  }

  private getAchievementPacks(): void {
    this.subs.sink = this.groupAchievementService.getAll().subscribe(groupAchievements => {
      this.groupAchievements = groupAchievements;
    }, error => {
      this.errorHandleService.handleError(error);
    });
  }
}
