import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {NotificationService} from '../../../shared/service/notification.service';
import {FilesHelper} from '../../../shared/helpers/file.helper';

@Component({
  selector: 'app-create-achievement-dialog',
  templateUrl: './create-achievement-dialog.component.html',
  styleUrls: ['./create-achievement-dialog.component.css']
})
export class CreateAchievementDialogComponent implements OnInit {
  createAchievementForm!: FormGroup;
  achievementFile!: any;

  private filesHelper = new FilesHelper();
  submitted = true;

  constructor(private formBuilder: FormBuilder,
              private notificationService: NotificationService,
              public dialogRef: MatDialogRef<CreateAchievementDialogComponent>) {
  }

  ngOnInit(): void {
  }

  public closeDialog(result: boolean): void {
    this.dialogRef.close(result);
  }

  public handleFileInputEvent(event: Event): void {
    const fileList  = (event.target as HTMLInputElement)?.files;
    if (fileList != null) {
      this.handleFileInput(fileList);
    }
  }

  public handleFileInput(files: FileList): void {
    this.achievementFile = this.filesHelper.handleFileInput(files);
  }

  public deleteFile(): void {
    this.achievementFile = null;
  }

  submit(): void {

  }
}
