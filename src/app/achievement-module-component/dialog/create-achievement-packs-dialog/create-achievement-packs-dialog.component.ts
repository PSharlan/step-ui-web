import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FilesHelper} from '../../../shared/helpers/file.helper';
import {Achievement, AchievementCreationRequest, actions, finishGroupAction} from '../../../shared/model/achievement/achievement-models';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GroupAchievement, GroupAchievementCreationRequest} from '../../../shared/model/achievement/group-achievement-models';
import {SubSink} from 'subsink';
import {AchievementGroupService} from '../../../shared/service/achievement/achievement-group.service';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {AchievementService} from '../../../shared/service/achievement/achievement.service';
import {forkJoin, Observable, throwError} from 'rxjs';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {NotificationService} from '../../../shared/service/notification.service';

@Component({
  selector: 'app-create-achievement-packs-dialog',
  templateUrl: './create-achievement-packs-dialog.component.html',
  styleUrls: ['./create-achievement-packs-dialog.component.css']
})
export class CreateAchievementPacksDialogComponent implements OnInit, OnDestroy {
  achievementPackNameFile!: any;
  actionsList: string[] = actions;
  fileHelper = new FilesHelper();
  groupAchievementForm!: FormGroup;
  mainAchievementForm!: FormGroup;
  mainForm!: FormGroup;
  backgroundColor = '#4898e2';
  submitted = false;
  private subs = new SubSink();

  constructor(public dialogRef: MatDialogRef<CreateAchievementPacksDialogComponent>,
              private fb: FormBuilder,
              private achievementGroupService: AchievementGroupService,
              private achievementService: AchievementService,
              private errorHandlerService: ErrorHandlerService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.generateForm();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private generateForm(): void {
    this.mainForm = this.fb.group({
      groupControl: this.fb.group({
        name: ['', Validators.required],
        backgroundColor: [this.backgroundColor, Validators.required]
      }),
      mainAchievementControl: this.fb.group({
        name: ['', Validators.required],
        action: [finishGroupAction]
      }),
      childAchievementControl: this.fb.array([])
    });
    this.addAchievement();
  }

  public addAchievement(): void {
    const childAchievementForm = this.fb.group({
      name: ['', Validators.required],
      action: ['', Validators.required],
      repeats: ['', Validators.required]
    });
    this.childAchievementControl.push(childAchievementForm);
  }

  public deleteAchievement(lessonIndex: number): void {
    this.childAchievementControl.removeAt(lessonIndex);
  }

  get childAchievementControl(): any {
    return this.mainForm.controls.childAchievementControl as FormArray;
  }

  public submit(): void {
    this.submitted = true;
    const achievementObservables: Observable<Achievement>[] = [];
    const groupAchievementRequest: GroupAchievementCreationRequest = {
      backgroundImg: this.mainForm.value.groupControl.backgroundColor,
      groupName: this.mainForm.value.groupControl.name
    };

    this.subs.sink = this.achievementGroupService.create(groupAchievementRequest)
      .pipe(
        catchError((error) => {
          this.errorHandlerService.handleError(error);
          this.submitted = false;
          return throwError(error);
        }),
        switchMap((groupAchievement: GroupAchievement) => {
          let index = 1;
          for (const achievement of this.mainForm.value.childAchievementControl) {
            const request: AchievementCreationRequest = {
              action: achievement.action,
              groupId: groupAchievement.id,
              img: 'some/url', // TODO add image url
              level: index,
              threshold: achievement.repeats,
              typeOfAchievement: achievement.name
            };
            achievementObservables.push(this.achievementService.create(request));
            index += 1;
          }

          const mainAchievementRequest: AchievementCreationRequest = {
            action: this.mainForm.value.mainAchievementControl.action,
            groupId: groupAchievement.id,
            img: 'some/url', // TODO add image url
            level: achievementObservables.length + 1,
            threshold: 1,
            typeOfAchievement: this.mainForm.value.mainAchievementControl.name
          };
          achievementObservables.push(this.achievementService.create(mainAchievementRequest));

          return forkJoin(achievementObservables);
        })
      ).subscribe((createdAchievements: Achievement[]) => {
        this.submitted = false;
        this.closeDialog(true);
        this.notificationService.showDefaultNotification('Набор достижений создан');
      });
  }

  public closeDialog(result: boolean): void {
    this.dialogRef.close(result);
  }

  public handleFileInputEvent(event: Event): void {
    const fileList = (event.target as HTMLInputElement)?.files;
    if (fileList != null) {
      this.handleFileInput(fileList);
    }
  }

  public handleFileInput(files: FileList): void {
    this.achievementPackNameFile = this.fileHelper.handleFileInput(files);
  }

  public deleteFile(): void {
    this.achievementPackNameFile = null;
  }

}
