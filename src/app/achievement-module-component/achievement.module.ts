import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyAchievementsComponent} from './my-achievements/my-achievements.component';
import {RouterModule} from '@angular/router';
import {AchievementModuleMenuComponent} from './achievement-module-menu/achievement-module-menu.component';
import {MyAchievementPacksComponent} from './my-achievement-packs/my-achievement-packs.component';
import {AppMaterialModule} from '../app-material.module';
import { CreateAchievementDialogComponent } from './dialog/create-achievement-dialog/create-achievement-dialog.component';
import { CreateAchievementPacksDialogComponent } from './dialog/create-achievement-packs-dialog/create-achievement-packs-dialog.component';
import { EditAchievementPacksDialogComponent } from './dialog/edit-achievement-packs-dialog/edit-achievement-packs-dialog.component';
import { EditAchievementDialogComponent } from './dialog/edit-achievement-dialog/edit-achievement-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';


@NgModule({
  declarations: [
    MyAchievementsComponent,
    AchievementModuleMenuComponent,
    MyAchievementPacksComponent,
    CreateAchievementDialogComponent,
    CreateAchievementPacksDialogComponent,
    EditAchievementPacksDialogComponent,
    EditAchievementDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: AchievementModuleMenuComponent, children: [
          {path: '', redirectTo: '/achievement-module/my-achievements', pathMatch: 'full'},
          {path: 'my-achievements', component: MyAchievementsComponent},
          {path: 'my-achievements-packs', component: MyAchievementPacksComponent}
        ]
      }
    ]),
    AppMaterialModule,
    ReactiveFormsModule,
    ColorPickerModule
  ]
})
export class AchievementModule {
}
