import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CreateAchievementDialogComponent} from '../dialog/create-achievement-dialog/create-achievement-dialog.component';
import {AchievementPreview} from '../../shared/model/achievement/achievement-models';
import {SubSink} from 'subsink';
import {AchievementService} from '../../shared/service/achievement/achievement.service';
import {PlayerService} from '../../shared/service/achievement/player.service';
import {forkJoin} from 'rxjs';
import {AuthService} from '../../shared/service/auth.service';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';

@Component({
  selector: 'app-my-achievements',
  templateUrl: './my-achievements.component.html',
  styleUrls: ['./my-achievements.component.css']
})
export class MyAchievementsComponent implements OnInit, OnDestroy {

  achievementViews: AchievementView[] = [];
  private subs = new SubSink();

  constructor(private dialog: MatDialog,
              private achievementService: AchievementService,
              private playerService: PlayerService,
              private authService: AuthService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.prepareAchievementViews();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  prepareAchievementViews(): void {
    this.subs.sink = forkJoin([
      this.achievementService.getAll(),
      this.playerService.getByUserId(this.authService.getUserId())
    ])
      .subscribe(([achievements, player]) => {
        console.log(player);
        console.log(achievements);
        const allAchievements = achievements.sort((a, b) => {
          return a.level - b.level;
        });
        for (let achievementIndex = 0; achievementIndex < allAchievements.length; achievementIndex++) {
          let isCompleted = false;
          let counter = 0;
          for (let userAchievementIndex = 0; userAchievementIndex < player?.achievements.length; userAchievementIndex++) {
            if (allAchievements[achievementIndex].id === player.achievements[userAchievementIndex].id) {
              isCompleted = true;
              break;
            }
          }
          if (!isCompleted) {
            for (let playerProgressIndex = 0; playerProgressIndex < player?.playersProgress.length; playerProgressIndex++) {
              if (player.playersProgress[playerProgressIndex].action === allAchievements[achievementIndex].action) {
                counter = player.playersProgress[playerProgressIndex].counterAction;
              }
            }
          }
          this.achievementViews.push(new AchievementView(allAchievements[achievementIndex], counter, isCompleted));
        }
      }, error => {
        this.errorHandlerService.handleError(error);
      });
  }

  openCreateAchievementDialog(): void {
    this.dialog.open(CreateAchievementDialogComponent, {
      panelClass: 'dialog-config',
      height: '700px',
    });
  }

  prepareWidth(counterAction: number, threshold: number): string {
    return (counterAction * 100 / threshold).toString() + '%';
  }
}

class AchievementView {

  public id: number;
  public action: string;
  public img: string;
  // public backgroundColor: string; // TODO add bgcolor
  public level: number;
  public threshold: number;
  public typeOfAchievement: string;
  public isCompleted: boolean;
  public counterAction: number;

  constructor(achievement: AchievementPreview, counter: number, isCompleted: boolean) {
    this.id = achievement.id;
    this.action = achievement.action;
    this.img = achievement.img;
    // this.backgroundColor = achievement // TODO add bgcolor to achievement
    this.level = achievement.level;
    this.threshold = achievement.threshold;
    this.typeOfAchievement = achievement.typeOfAchievement;
    this.isCompleted = isCompleted;
    this.counterAction = counter;
  }
}
