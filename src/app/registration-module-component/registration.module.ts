import {NgModule} from '@angular/core';
import {RegistrationModuleMenuComponent} from './registration-module-menu/registration-module-menu.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TeacherComponent} from './sign-up/teacher/teacher.component';
import {StudentComponent} from './sign-up/student/student.component';
import {AppMaterialModule} from '../app-material.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    RegistrationModuleMenuComponent,
    SignInComponent,
    SignUpComponent,
    TeacherComponent,
    StudentComponent
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '', component: RegistrationModuleMenuComponent, children: [
          {path: '', redirectTo: 'signin'},
          {path: 'signin', component: SignInComponent},
          {path: 'signup', component: SignUpComponent},
          {path: 'student', component: StudentComponent},
          {path: 'teacher', component: TeacherComponent}
        ]
      }
    ]),
    CommonModule,
    AppMaterialModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})

export class RegistrationModule {
}
