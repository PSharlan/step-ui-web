import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration-module-menu',
  templateUrl: './registration-module-menu.component.html',
  styleUrls: ['./registration-module-menu.component.css']
})
export class RegistrationModuleMenuComponent implements OnInit {

  constructor(public route: Router) {
  }

  ngOnInit(): void {
  }

}
