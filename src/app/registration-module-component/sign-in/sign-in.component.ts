import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {SubSink} from 'subsink';
import {LoginRequest} from '../../shared/model/login-request-model';
import {AuthService} from '../../shared/service/auth.service';
import {catchError} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {ErrorHandlerService} from '../../shared/service/error-handler.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  submitted = false;
  hidePassword = true;
  private subs = new SubSink();

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private auth: AuthService,
              private errorHandlerService: ErrorHandlerService,
              private router: Router) {
    this.loadImages();
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  private loadImages(): void {
    this.matIconRegistry.addSvgIcon(
      'hide-password',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/hide-password.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'show-password',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/show-password.svg')
    );
  }

  submit(): void {
    this.submitted = true;
    const request: LoginRequest = {
      email: this.form.value.email,
      password: this.form.value.password
    };
    this.subs.sink = this.auth.login(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 401) {
            this.errorHandlerService.handleError('Неверный логин или пароль');
            this.submitted = false;
          }
          return throwError(error);
        })
      )
      .subscribe((response) => {
        this.auth.saveToken(response);
        this.auth.saveUserId(response);
        this.auth.saveUserRole(response);
        this.router.navigate(['/']);
      });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
