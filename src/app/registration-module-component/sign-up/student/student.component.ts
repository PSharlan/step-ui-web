import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {MustMatch} from '../../../shared/helpers/validators/confirm-password.helper';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {GroupService} from '../../../shared/service/account/group.service';
import {catchError, tap} from 'rxjs/operators';
import {Group} from '../../../shared/model/account/group-models';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {StudentCreationRequest} from '../../../shared/model/account/student-models';
import {StudentService} from '../../../shared/service/account/student.service';
import {NotificationService} from '../../../shared/service/notification.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {SubSink} from 'subsink';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  submitted = false;
  groupId!: number;
  private subs = new SubSink();

  constructor(private formBuilder: FormBuilder,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private groupService: GroupService,
              private studentService: StudentService,
              private notificationService: NotificationService,
              private errorHandlerService: ErrorHandlerService) {
    this.loadImage();
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.formBuilder.group({
      groupNumber: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  private loadImage(): void {
    this.matIconRegistry.addSvgIcon(
      'clarity-help-line',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/clarity-help-line.svg')
    );
  }

  public getGroupId(name: string): void {
    this.groupService.getByName(name)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.form.controls.groupNumber.setErrors({incorrect: true});
          }
          return throwError(error);
        }),
        tap((group: Group) => {
          this.groupId = group.id;
          this.form.controls.groupNumber.setErrors(null);
        })).subscribe();
  }

  submit(formDirective: FormGroupDirective): void {
    this.submitted = true;
    const request: StudentCreationRequest = {
      groupId: this.groupId,
      name: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.email,
      password: this.form.value.password,
      rating: 0 // TODO: remove with next step-account version
    };
    this.subs.sink = this.studentService.create(request).subscribe(
      () => {
        this.submitted = false;
        formDirective.resetForm();
        this.form.reset();
        this.notificationService.showDefaultNotification('Аккаунт успешно создан. Ждите подтверждения');
      },
      (error) => {
        this.submitted = false;
        this.errorHandlerService.handleError(error);
      });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
