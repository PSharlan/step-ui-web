import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {MustMatch} from '../../../shared/helpers/validators/confirm-password.helper';
import {TeacherCreationRequest} from '../../../shared/model/account/teacher-models';
import {TeacherService} from '../../../shared/service/account/teacher.service';
import {NotificationService} from '../../../shared/service/notification.service';
import {ErrorHandlerService} from '../../../shared/service/error-handler.service';
import {SubSink} from 'subsink';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  submitted = false;
  private subs = new SubSink();

  constructor(private formBuilder: FormBuilder,
              private teacherService: TeacherService,
              private notificationService: NotificationService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.formBuilder.group({
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  submit(formDirective: FormGroupDirective): void {
    this.submitted = true;
    const request: TeacherCreationRequest = {
      name: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.email,
      password: this.form.value.password
    };
    this.subs.sink = this.teacherService.create(request).subscribe(
      () => {
        formDirective.resetForm();
        this.form.reset();
        this.submitted = false;
        this.notificationService.showDefaultNotification('Аккаунт успешно создан. Ждите подтверждения');
      },
      error => {
        this.submitted = false;
        this.errorHandlerService.handleError(error);
      });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
