import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './shared/components/main-layout/main-layout.component';
import {RegistrationLayoutComponent} from './shared/components/registration-layout/registration-layout.component';
import {ActiveTestLayoutComponent} from './shared/components/active-test-layout/active-test-layout.component';
import {CreateTestComponent} from './test-module-component/create-test/create-test.component';
import {CreateQuestionDialogComponent} from './test-module-component/create-question-dialog/create-question-dialog.component';
import {CreateRoadmapComponent} from './roadmap-module-component/create-roadmap/create-roadmap.component';
import {AuthGuard} from './shared/service/auth.guard';

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'test-module',
        loadChildren: () => import('./test-module-component/test.module').then(m => m.TestModule)
      },
      {
        path: 'roadmap-module',
        loadChildren: () => import('./roadmap-module-component/roadmap.module').then(m => m.RoadmapModule)
      },
      {
        path: 'group-module',
        loadChildren: () => import('./group-students-module-component/group.module').then(m => m.GroupModule)
      },
      {
        path: 'achievement-module',
        loadChildren: () => import('./achievement-module-component/achievement.module').then(m => m.AchievementModule)
      }
    ]
  },
  {
    path: 'account',
    component: RegistrationLayoutComponent,
    loadChildren: () => import('./registration-module-component/registration.module').then(m => m.RegistrationModule)
  },
  {
    path: 'active-test',
    canActivate: [AuthGuard],
    component: ActiveTestLayoutComponent,
    loadChildren: () => import('./active-test-module-component/active-test.module').then(m => m.ActiveTestModule)
  },
  {
    path: 'create-test',
    canActivate: [AuthGuard],
    component: CreateTestComponent
  },
  {
    path: 'create-question',
    canActivate: [AuthGuard],
    component: CreateQuestionDialogComponent
  },
  {
    path: 'create-roadmap',
    canActivate: [AuthGuard],
    component: CreateRoadmapComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
