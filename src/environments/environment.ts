// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  testModuleUrl: (window as any)['env']?.TEST_MODULE_URL_PREFIX || 'http://localhost:8082/',
  accountModuleUrl: (window as any)['env']?.ACCOUNT_MODULE_URL_PREFIX || 'http://localhost:8081/',
  homeworkModuleUrl: (window as any)['env']?.HOMEWORK_MODULE_URL_PREFIX || 'http://localhost:8084/',
  achievementModuleUrl: (window as any)['env']?.ACHIEVEMENT_MODULE_URL_PREFIX || 'http://localhost:8087/',
  roadmapModuleUrl: (window as any)['env']?.ROADMAP_MODULE_URL_PREFIX || 'http://localhost:8086/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
