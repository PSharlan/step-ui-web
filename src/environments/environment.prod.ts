export const environment = {
  production: true,
  testModuleUrl: (window as any).env?.TEST_MODULE_URL_PREFIX || 'http://localhost:8082/',
  accountModuleUrl: (window as any).env?.ACCOUNT_MODULE_URL_PREFIX || 'http://localhost:8081/',
  homeworkModuleUrl: (window as any).env?.HOMEWORK_MODULE_URL_PREFIX || 'http://localhost:8084/',
  achievementModuleUrl: (window as any).env?.ACHIEVEMENT_MODULE_URL_PREFIX || 'http://localhost:8087/',
  roadmapModuleUrl: (window as any).env?.ROADMAP_MODULE_URL_PREFIX || 'http://localhost:8086/',
};
